#
#    Copyright (C) 2020 Dyadic Pty Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#
# Generated from hmc833_qt.org
#

from typing import (
    Dict, Optional
)

import serial
import asyncio

from rfblocks import (
    hmc833, HMC833Controller, create_serial
)

from qasync import (
    asyncSlot
)

from PyQt5.QtWidgets import (
    QWidget, QGroupBox, QVBoxLayout, QHBoxLayout, QFormLayout,
    QLabel, QDoubleSpinBox, QButtonGroup, QRadioButton, QCheckBox,
    QPushButton, QErrorMessage
)

from PyQt5.QtCore import (
    Qt, QObject
)


class HMC833Module(QObject):

    FREQ_SUFFIX: str = ' MHz'
    LEVEL_SUFFIX: str = ' dBm'

    def __init__(self,
                 app: QWidget,
                 controller: HMC833Controller) -> None:
        """Create an instance of ``HMC833Module``.

        :param app: The parent application for the HMC833 module display.
        :type app: QWidget
        :param controller: HMC833 PLO controller
        :type controller: :py:class:`rfblocks.HMC833Controller`
        """
        super().__init__(app)
        self._app: QWidget = app
        self._controller: HMC833Controller = controller
        self._group_box = None
        self._freq_box = None
        self._level_box = None
        self._buffer_gain_widgets = {}
        self._has_divider_gain = True
        self._divider_gain_widgets = {}

    @property
    def ctl(self) -> HMC833Controller:
        """Return a reference to the :py:class:`HMC833Controller`.
        """
        return self._controller

    def create_ploconfig_group(self):
        self._group_box = QGroupBox("{} PLO :".format(
            self.ctl._controller_id))
        self._group_box.setLayout(self.create_ploconfig_layout())
        return self._group_box
    
    def create_ploconfig_layout(
            self,
            has_level: bool = False,
            level_delegate: Optional[QObject] = None,
            has_divider_gain: bool = True,
            has_configure: bool = True):
        vbox = QVBoxLayout()
        hbox = QHBoxLayout()
        fbox = QFormLayout()
    
        self._freq_box = QDoubleSpinBox()
        self._freq_box.setRange(hmc833.MIN_FREQUENCY, hmc833.MAX_FREQUENCY)
        self._freq_box.setDecimals(3)
        self._freq_box.setStyleSheet("""QDoubleSpinBox {
                 font-size: 25pt; }""")
        self._freq_box.setValue(self.ctl.freq)
        self._freq_box.setSuffix(HMC833Module.FREQ_SUFFIX)
        self._freq_box.valueChanged.connect(self.set_freq)
        fbox.addRow(QLabel("Frequency:"), self._freq_box)
    
        self.ctl.freq_changed.connect(self.update_plo_freq)
    
        if has_level is True:
            self._level_box = QDoubleSpinBox()
            self._level_box.setDecimals(2)
            self._level_box.setStyleSheet("""QDoubleSpinBox {
                font-size: 25pt; }""")
            self._level_box.setValue(level_delegate.level)
            self._level_box.setSuffix(HMC833Module.LEVEL_SUFFIX)
            self._level_box.valueChanged.connect(level_delegate.set_level)
            fbox.addRow(QLabel("Level:"), self._level_box)
    
        hbox2 = QHBoxLayout()
        self._buffer_gain_widgets = {}
        bufferGroup = QButtonGroup(hbox2)
        rb = QRadioButton("-9 dB")
        bufferGroup.addButton(rb)
        rb.gain = hmc833.OutputBufferGain.MAXGAIN_MINUS_9DB
        rb.toggled.connect(lambda state, w=rb: self.set_buffer_gain(w, state))
        hbox2.addWidget(rb)
        self._buffer_gain_widgets[rb.gain] = rb
        rb = QRadioButton("-6 dB")
        bufferGroup.addButton(rb)
        rb.gain = hmc833.OutputBufferGain.MAXGAIN_MINUS_6DB
        rb.toggled.connect(lambda state, w=rb: self.set_buffer_gain(w, state))
        hbox2.addWidget(rb)
        self._buffer_gain_widgets[rb.gain] = rb
        rb = QRadioButton("-3 dB")
        bufferGroup.addButton(rb)
        rb.gain = hmc833.OutputBufferGain.MAXGAIN_MINUS_3DB
        rb.toggled.connect(lambda state, w=rb: self.set_buffer_gain(w, state))
        hbox2.addWidget(rb)
        self._buffer_gain_widgets[rb.gain] = rb
        rb = QRadioButton("Max gain")
        bufferGroup.addButton(rb)
        rb.gain = hmc833.OutputBufferGain.MAXGAIN
        rb.toggled.connect(lambda state, w=rb: self.set_buffer_gain(w, state))
        hbox2.addWidget(rb)
        self._buffer_gain_widgets[rb.gain] = rb
        fbox.addRow(QLabel("Buffer gain:"), hbox2)
    
        self._buffer_gain_widgets[self.ctl.buffer_gain].setChecked(True)
        self.ctl.bufgain_changed.connect(self.update_buffer_gain)
    
        self._has_divider_gain = has_divider_gain
        if has_divider_gain is True:
            hbox3 = QHBoxLayout()
            self._divider_gain_widgets = {}
            dividerGroup = QButtonGroup(hbox3)
            rb = QRadioButton("-3 dB")
            dividerGroup.addButton(rb)
            rb.gain = hmc833.DividerGain.MAXGAIN_MINUS_3DB
            rb.toggled.connect(
                lambda state, w=rb: self.set_divider_gain(w, state))
            hbox3.addWidget(rb)
            self._divider_gain_widgets[rb.gain] = rb
            rb = QRadioButton("Max gain")
            dividerGroup.addButton(rb)
            rb.gain = hmc833.DividerGain.MAXGAIN
            rb.toggled.connect(
                lambda state, w=rb: self.set_divider_gain(w, state))
            hbox3.addWidget(rb)
            self._divider_gain_widgets[rb.gain] = rb
            fbox.addRow(QLabel("Divider gain:"), hbox3)
    
            self._divider_gain_widgets[self.ctl.divider_gain].setChecked(True)
            self.ctl.divgain_changed.connect(self.update_divider_gain)
    
        self._vco_mute_box = QCheckBox("")
        self._vco_mute_box.setChecked(self.ctl.vco_mute)
        self._vco_mute_box.stateChanged.connect(self.set_vco_mute)
        fbox.addRow(QLabel("Mute VCO:"), self._vco_mute_box)
    
        self.ctl.vco_mute_changed.connect(self.update_vco_mute)
    
        self._locked_box = QCheckBox("")
        self._locked_box.setChecked(False)
        self._locked_box.setAttribute(Qt.WA_TransparentForMouseEvents, True)
        self._locked_box.setFocusPolicy(Qt.NoFocus)
        fbox.addRow(QLabel("PLO Locked:"), self._locked_box)
    
        self.ctl.lock_status_changed.connect(self.update_locked_status)
    
        hbox.addLayout(fbox)
        hbox.addStretch()
        vbox.addLayout(hbox)
    
        if has_configure is True:
            hbox2 = QHBoxLayout()
            hbox2.addStretch(1)
            self._config_btn = QPushButton("Configure")
            self._config_btn.clicked.connect(self.configure)
            hbox2.addWidget(self._config_btn)
            vbox.addLayout(hbox2)
    
        return vbox
    
    def set_freq(self, f):
        self.ctl.freq = f
    
    def enable_buffer_gain(self, state):
        for rb in self._buffer_gain_widgets.values():
            rb.setEnabled(state)
    
    def set_buffer_gain(self, rb, state):
        if state:
            self.ctl.buffer_gain = rb.gain
    
    def enable_divider_gain_(self, state):
        for rb in self._divider_gain_widgets.values():
            rb.setEnabled(state)
    
    def set_divider_gain(self, rb, state):
        if state:
            self.ctl.divider_gain = rb.gain
    
    def set_vco_mute(self, state):
        if state == Qt.Checked:
            self.ctl.vco_mute = True
        else:
            self.ctl.vco_mute = False

    @property
    def enabled(self) -> bool:
        """Enable/disable the on screen synthesizer UI components.
        """
        if self._group_box:
            return self._group_box.isEnabled()
        else:
            return False

    @enabled.setter
    def enabled(self, en: bool) -> None:
        if self._group_box:
            self._group_box.setEnabled(en)

    def update_plo_freq(self, f: float) -> None:
        """Update the displayed PLO output frequency.
        """
        self._freq_box.setValue(f)

    def update_buffer_gain(self, gain: hmc833.OutputBufferGain) -> None:
        """Update the displayed output buffer gain setting.
        """
        self._buffer_gain_widgets[gain].setChecked(True)

    def update_divider_gain(self, gain: hmc833.DividerGain) -> None:
        """Update the displayed divider output stage gain setting.
        """
        self._divider_gain_widgets[gain].setChecked(True)

    def update_vco_mute(self, mute: bool) -> None:
        """Update the displayed VCO mute status.
        """
        self._vco_mute_box.setChecked(mute)

    def update_locked_status(self, locked: bool) -> None:
        """Update the displayed PLO locked status.
        """
        self._locked_box.setChecked(locked)

    def dump_config(self) -> Dict:
        """Return the current configuration for this synthesizer
           module.

        :return: A dictionary containing the current synthesizer
            module configuration.
        """
        return self.ctl.dump_config()

    def load_config(self, config: Dict) -> None:
        """Set the current configuration for this synthesizer module.

        :param config: A dictionary containing the synthesizer module
            configuration to be set.
        :type config: Dict
        """
        self.ctl.load_config(config)

    @asyncSlot()
    async def configure(self) -> None:
        """Update the PLO hardware using the currently set configuration.
        """
        try:
            with create_serial(self._app.ctl_device,
                               self._app.baudrate) as ser:
                loop = asyncio.get_event_loop()
                await loop.run_in_executor(None, self.configure_hw, ser)
        except serial.serialutil.SerialException as se:
            error_dialog = QErrorMessage(self._app)
            error_dialog.showMessage(str(se))

    def initialize_hw(self, ser: serial.Serial) -> None:
        """Initialize the PLO board hardware.
        """
        _ = self.ctl.initialize(ser)

    def configure_hw(self, ser: serial.Serial) -> None:
        """Configure the PLO board hardware.
        """
        _ = self.ctl.configure(ser)

    def initialize_ui(self) -> None:
        """Initialize the user interface.
        """
        self.configure_ui(self.ctl._initial_config)

    def configure_ui(self, config: Dict) -> None:
        self._freq_box.setValue(config['freq'])
        self._buffer_gain_widgets[config['bufgain']].setChecked(True)
        if self._has_divider_gain is True:
            self._divider_gain_widgets[config['divgain']].setChecked(True)
        if config['vco_mute'] is True:
            self._vco_mute_box.setChecked(True)
        else:
            self._vco_mute_box.setChecked(False)
