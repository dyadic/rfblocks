# A Python Package for [RF Blocks](https://rfblocks.org/articles/RF-Blocks.html) Hardware Control

## Documentation

Documentation for the package is available at
https://rfblocks.org/rfblocks-sw/index.html

## License

[GNU Lesser General Public License](https://rfblocks.org/rfblocks-sw/license.html)
