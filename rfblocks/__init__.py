"""`rfblocks` - Control RF hardware devices

    Copyright (C) 2020 Dyadic Pty Ltd

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

__version__ = '0.1.0'
__author__ = 'Dyadic Pty Ltd'

from .ad9552 import (
    DividerRangeException, ad9552
)
from .ad9913 import (
    ModulusConstraintException, ad9913
)
from .hmc833 import (
    FrequencyRangeException, UnknownGainSettingException, hmc833
)
from .pe43711 import (
    AttenuationRangeException, pe43711
)
from .bridge_coupler import (
    BridgeCoupler
)
from .rpdac_controller import (
    RPDacController
)
from .adf4351 import (
    adf4351
)

from typing import Dict
import os
import json
import serial

DEFAULT_SOCKET_PORT: int = 7000
DEFAULT_SOCKET_URL: str = "socket://localhost:7000"
DEFAULT_BAUDRATE = 1500000

def parse_hwconf(conf: Dict) -> Dict:
    """Parse the specified hardware configuration file.

    Parameters
    ----------
    conf : dict
        A description of the rfblocks hardware configuration.
        This might be loaded from a JSON description for example.

    Returns
    -------
    dict
        A dictionary of RF Blocks device instances for the control of
        the hardware devices specified in the configuration file.
        The dictionary is keyed using the device names given in the
        configuration file.
    """
    # Construct device instances using the configuration.
    devices = {}
    for device_name, device_desc in conf['devices'].items():
        device_class = getattr(sys.modules['rfblocks.'+device_desc['type']],
                               device_desc['type'])
        devices[device_name] = device_class(**device_desc['params'])
    return devices

def list_available_serial_ports() -> None:
    """
    """
    from serial.tools.list_ports import comports

    for port_info in comports():
        print(port_info.device)
        if port_info.description not in ['n/a']:
            descr = port_info.description
        else:
            descr = os.path.splitext(port_info.device)[1][1:]
        print("  Description:   {}".format(descr))            
        print("  Hw Id:         {}".format(port_info.hwid))
        print("  Serial number: {}".format(port_info.serial_number))

def create_serial(device,
                  baudrate=1500000,
                  **kwargs):
    """Create a Serial instance for access to a specified serial port device.

    Parameters
    ----------
    device : str
        The serial port device (e.g. `'/dev/ttyACM0'`).
    baudrate : int, optional
        Default: 1500000
    """
    return serial.serial_for_url(device, baudrate, **kwargs)

def write_serial_cmd(ser, cmd):
    """Write a controller command string to the specified serial device.

    Parameters
    ----------
    ser : serial.Serial
        A Serial instance providing access to the serial device associated with
        the controller.
    cmd : str
        The command string to send to the controller.

    Returns
    -------
    str
        The single character response as received from the controller.
    """
    ser.write(bytes('{}\r'.format(cmd), "utf8"))
    resp = ser.read(1)
    return resp

def query_serial_cmd(ser, cmd):
    """Write a controller command string to the specified serial device.

    After sending the command string bytes are read from the controller
    until the terminating '.' is encountered.

    Parameters
    ----------
    ser : serial.Serial
        A Serial instance providing access to the serial device associated with
        the controller.
    cmd : str
        The command string to send to the controller.

    Returns
    -------
    list
        A list containing data bytes received from the controller including
        the terminating '.' character.
    """
    resp = []
    resp.append(write_serial_cmd(ser, cmd))
    while resp[-1] != b'.':
        resp.append(ser.read(1))
    return resp

def write_cmd(iodev, cmd):
    """Write a controller command to a given output device.

    This is a convenience function to allow for command strings to be
    written either to a hardware controller or to a log file, `sys.stdout`,
    or some other `write`-able object.

    Parameters
    ----------
    iodev :
        An output device.  This may either be an instance of `serial.Serial`
        or some other device instance which implements the `write` operation
        (for example, `sys.stdout`).
    cmd : str
        The command string to output

    Returns
    -------
    str
        If `iodev` is an instance of `serial.Serial` the returned string will
        be single character response as received from the connected
        controller.  If `iodev` is some other type of `write`-able object,
        and empty string is returned.
    """
    if (isinstance(iodev, serial.Serial) or
        isinstance(iodev, serial.urlhandler.protocol_socket.Serial)):
        return write_serial_cmd(iodev, cmd)
    else:
        iodev.write(cmd+'\n')
        return ''

def query_cmd(iodev, cmd):
    """Write a controller command string to the given output device.

    After sending the command string bytes are read from the controller
    until the terminating '.' is encountered.

    This is a convenience function to allow for command strings to be
    written either to a hardware controller or to a log file, `sys.stdout`,
    or some other `write`-able object.

    Parameters
    ----------
    ser : serial.Serial
        A Serial instance providing access to the serial device associated with
        the controller.
    cmd : str
        The command string to send to the controller.

    Returns
    -------
    list
        A list containing data bytes received from the controller including
        the terminating '.' character.
    """
    if (isinstance(iodev, serial.Serial) or
        isinstance(iodev, serial.urlhandler.protocol_socket.Serial)):
        return query_serial_cmd(iodev, cmd)
    else:
        iodev.write(cmd+'\n')
        return [b'0']

from .log_detector import (
    CalibrationDataError, InvalidCalibrationDataError,
    DetectorReadError, LogDetector
)
from .ad9913_controller import (
    AD9913Controller
)
from .ad9552_controller import (
    AD9552Channel, AD9552Controller
)
from .pwr_detector_controller import (
    CalibrationRangeError, PwrDetectorController
)
from .pe43711_controller import (
    PE43711Controller
)
from .hmc833_controller import (
    HMC833Controller
)

from .pe42420 import (
    pe42420
)
from .pe42420_controller import (
    PE42420Controller
)
from .hmc1031 import (
    hmc1031
)
