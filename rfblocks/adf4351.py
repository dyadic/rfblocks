# `adf4351` - Encapsulates control for the ADF4351 frequency synthesizer
#             demo board.
#
#    Copyright (C) 2022 Dyadic Pty Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import (
    List, Dict, Tuple, Optional
)
import math
from enum import IntFlag
from fractions import Fraction

class FrequencyRangeException(Exception):
    """Raised when a requested output frequency is out of range.
    """
    pass


class UnknownGainSettingException(Exception):
    """Raised when an invalid gain setting is specified.
    """
    pass


class adf4351:

    """Ecapsulates control for the ADF4351 frequency synthesizer device.
    """

    MIN_FUNDAMENTAL: float = 2200.0
    "The minimum VCO fundamental frequency"
    MAX_FUNDAMENTAL: float = 4400.0
    "The maximum VCO fundamental frequency"

    MIN_FREQUENCY: float = 25.0
    "The minimum synthesizer output frequency"
    MAX_FREQUENCY: float = 4400.0
    "The maximum synthesizer output frequency"

    MAX_MODULUS: int = 4095
    MIN_INTDIV: int = 23
    MAX_INTDIV: int = 524284
    MAX_INTPDF: float = 90.0  # Band select mode == 0
    # MAX_INTPDF: float = 45.0  # Band select mode == 1
    MAX_REF_WITH_DOUBLER: float = 30.0

    MIN_CP_GAIN: int = 0
    MAX_CP_GAIN: int = 15

    DEFAULT_REF_FREQ: float = 25.0

    DEFAULT_CP_GAIN: int = 7

    DIVIDER_VALUES: List[int] = [1, 2, 4, 8, 16, 32, 64]
    "Legal RF output divider values"

    class OutputPowerGain(IntFlag):
        """An `enum` containing the possible output power settings.
        """
        MAXGAIN_MINUS_9DB = 0b00
        MAXGAIN_MINUS_6DB = 0b01
        MAXGAIN_MINUS_3DB = 0b10
        MAXGAIN = 0b11

        @classmethod
        def has_value(cls, value):
            return value in cls._value2member_map_
        
    class Prescaler(IntFlag):
        """An `enum` containing the possible prescaler settings.
        """
        FourOverFive = 0b00
        EightOverNine = 0b01


    def __init__(self,
                 le: str = None,
                 ld: str = None,
                 muxout: str = None,
                 pdrf: str = None,
                 fref: float = DEFAULT_REF_FREQ,
                 ref_div: int = 1,
                 ref_divby2: bool = False,
                 ref_double: bool = False,
                 cp_gain: int = DEFAULT_CP_GAIN) -> None:
        """
        :param le: The ADF4351 serial port load enable controller pin.
        :type le: str
        :param ld: The ADF4351 lock detect controller pin.
        :type ld: str
        :param muxout: The ADF4351 multiplexer output controller pin.
        :type muxout: str
        :param pdrf: The ADF4351 RF power down controller pin.
        :type pdrf: str
        :param fref: The input reference frequency in MHz.
            Default: 25 MHz.
        :type fref: float
        :param ref_div: The input reference divider value (1..1,023),
            default 1.
        :type ref_div: int
        :param ref_divby2: If set to True a divide-by-2 toggle flip-flop
            is inserted between the R counter and the PFD.
        :type ref_divby2: bool
        :param ref_double: If set to True both the rising and falling edges
            of the reference signal become active at the PFD.  This effectively
            doubles the reference frequency.
        :type ref_double: bool
        """
        self.le = le.upper() if le else le
        self.ld = ld.upper() if ld else ld
        self.muxout = muxout.upper() if muxout else muxout
        self.pdrf = pdrf.upper() if pdrf else pdrf

        self._fref: float = fref        
        self._ref_doubler: int = int(ref_double)
        self._ref_divider: int = ref_div
        self._ref_divby2: int = int(ref_divby2)
        self._n_int = None
        self._n_frac = None
        self._n_mod = None
        self._div_select = None
        self._cp_gain = cp_gain
        self._rf_output_enable = True
        self._pwr_gain = adf4351.OutputPowerGain.MAXGAIN_MINUS_9DB
        self._prescaler = adf4351.Prescaler.FourOverFive
        self._pwr_down: int = 0

    def __repr__(self) -> str:
        return "{}({!r})".format(self.__class__.__name__, vars(self))

    def pin_config(self) -> str:
        """Initialize controller pin configuration.

        :return: A string specifying the commands required to initialize the
            connected controller pins.

        """
        cmd = ''
        if self.le:
            cmd += 'O{}:L{}:'.format(self.le, self.le)
        if self.ld:
            cmd += 'I{}:'.format(self.ld)
        if self.muxout:
            cmd += 'I{}:'.format(self.muxout)
        if self.pdrf:
            # By default the RF output is un-muted
            cmd += 'O{}:H{}:'.format(self.pdrf, self.pdrf)
        return cmd
    
    @property
    def fref(self):
        return self._fref

    @property
    def ref_doubler(self):
        if self._ref_doubler is True:
            return 1
        else:
            return 0

    @property
    def refdiv(self) -> int:
        return self._ref_divider

    @refdiv.setter
    def refdiv(self, refdiv: int) -> None:
        self._ref_divider = refdiv

    @property
    def ref_divby2(self) -> int:
        if self._ref_divby2 is True:
            return 1
        else:
            return 0

    @property
    def n_int(self) -> int:
        return self._n_int

    @property
    def n_frac(self) -> int:
        return self._n_frac

    @property
    def n_mod(self) -> int:
        return self._n_mod

    @property
    def div_select(self) -> int:
        return self._div_select

    @property
    def pfd_freq(self) -> float:
        """Calculate the phase frequency detector frequency
        """
        return self.fref * ((1 + self.ref_doubler) /
                            (self.refdiv * (1 + self.ref_divby2)))

    @property
    def cp_gain(self) -> int:
        """The current charge pump gain
        """
        return self._cp_gain

    @cp_gain.setter
    def cp_gain(self, gain: int) -> None:
        if gain < adf4351.MIN_CP_GAIN:
            self._cp_gain = adf4351.MIN_CP_GAIN
        if gain > adf4351.MAX_CP_GAIN:
            self._cp_gain = adf4351.MAX_CP_GAIN
        else:
            self._cp_gain = gain

    @property
    def pwr_gain(self) -> OutputPowerGain:
        """
        """
        return self._pwr_gain

    @pwr_gain.setter
    def pwr_gain(self, gain: OutputPowerGain) -> None:
        if adf4351.OutputPowerGain.has_value(gain):
            self._pwr_gain = gain
        else:
            raise UnknownGainSettingException

    @property
    def pwr_down(self) -> int:
        return self._pwr_down

    @pwr_down.setter
    def pwr_down(self, enable: int) -> None:
        if enable > 0:
            self._pwr_down = 1
        else:
            self._pwr_down = 0

    @property
    def rf_output_enable(self) -> bool:
        return self._rf_output_enable

    @rf_output_enable.setter
    def rf_output_enable(self, enable: bool) -> None:
        self._rf_output_enable = enable

    @property
    def prescaler(self) -> Prescaler:
        return self._prescaler

    def rf_divider(self, fout):
        try:
            return [d for d in adf4351.DIVIDER_VALUES
                    if fout*d >= adf4351.MIN_FUNDAMENTAL
                    and fout*d <= adf4351.MAX_FUNDAMENTAL][-1]
        except IndexError:
            # The specified fout is out of range
            raise FrequencyRangeException from None

    def divider_values(self, fout: float) -> Tuple[int, int, int, int]:
        """Calculate the N divider values.

        :param fout: The desired output frequency in MHz.
        :type fout: float

        :returns: A tuple containing the parameters for the fractional
            frequency tuning: ``(n_int, n_mod, n_frac, divider)``
            where:

            ``n_int``: is the integer division ratio, a number between
            20 and 524,284.

            ``n_mod`` : is the fractional modulus, from 0 to 4095.

            ``n_frac`` : is the fractional numerator, from 0 to n_mod-1.

            ``divider`` : the required setting for the output frequency
            divider. If this is 1 then the divider is not used.

        :raises: FrequencyRangeException
            If the specified output frequency is out of range.

        """
        divider = 1
        n_mod = 1
        n_frac = 0
        prescaler = adf4351.Prescaler.FourOverFive
        min_n_int = self.MIN_INTDIV
        if fout < adf4351.MIN_FREQUENCY or fout > adf4351.MAX_FREQUENCY:
            raise FrequencyRangeException
        if fout > 3600.0:
            prescaler = adf4351.Prescaler.EightOverNine
            min_n_int = 75
        if fout < adf4351.MIN_FUNDAMENTAL:
            divider = self.rf_divider(fout)

        div_factor = fout / (self.pfd_freq / divider)
        n_int = math.floor(div_factor)
        if n_int < min_n_int or n_int > self.MAX_INTDIV:
            raise FrequencyRangeException
        frac = div_factor - n_int

        # If frac is larger than the available resolution, determine the
        # fractional division ratio.
        if frac > 1/self.MAX_MODULUS:
            f = Fraction(f"{frac:.5f}")
            n_mod = f.denominator
            n_frac = f.numerator

        self._prescaler = prescaler
        
        self._n_int = n_int
        self._n_mod = n_mod
        self._n_frac = n_frac
        self._div_select = self.DIVIDER_VALUES.index(divider)

    def config_reg5(self) -> str:
        # Lock detect pin operation: 0x01 (digital lock detect)
        #  0000_0000_0101_1000_0000_0000_0000_0101
        #            \/ \_/
        #            |   |
        #            |   reserved
        #            Lock detect pin operation
        #
        cmd = 'W00,58,00,05:H{}:L{}:'.format(self.le, self.le)
        return cmd

    def config_reg4(self, divider_select) -> str:
        #
        # 0000_0000_1000_1100_1000_0000_0011_1100
        #           |\_/ \_______/ |||| \/|\_/\_/
        #           | |      |     |||| | | |  |
        #           | |      |     |||| | | |  Reg_04
        #           | |      |     |||| | | output pwr
        #           | |      |     |||| | rf output enable
        #           | |      |     |||| aux output pwr
        #           | |      |     |||aux output enable
        #           | |      |     ||aux output select
        #           | |      |     |mute till lock detect
        #           | |      |     vco pwr down
        #           | |      band select clk divider
        #           | rf divider select
        #           feedback select

        # Feedback select is 0b1 (fundamental)
        # The band select clock divider is set to 200 (0xC8)
        reg_04 = '{:06X}'.format(0x008C8004 | (int(self.pwr_gain) << 3)
                                 | (int(self.rf_output_enable) << 5)
                                 | (divider_select << 20))
        cmd = 'W00,{},{},{}:H{}:L{}:'.format(reg_04[0:2], reg_04[2:4], reg_04[4:6],
                                             self.le, self.le)
        return cmd

    def config_reg3(self, n_frac: int) -> str:
        #
        # 0000_0000_0000_0000_0000_0100_1011_0011
        #           |||\_/||\_/\_____________/\_/
        #           ||| | || |        |        |
        #           ||| | || |        |        Reg_03
        #           ||| | || |        clock divider value
        #           ||| | || clock divider mode
        #           ||| | |reserved
        #           ||| | cycle slip reduction
        #           ||| reserved
        #           ||charge cancel
        #           |antibacklash
        #           band select clock mode

        # Clock divider value: 150
        # Clock divider mode: Clock divider off (0b00)
        # Band select clock mode: Low (0b0)
        # Cycle slip reduction: disabled (0b0)
        if n_frac == 0:
            # integer mode
            # Enable charge cancellation and set antibacklash to 3nS (0b1)
            reg_03 = '{:06X}'.format(0x000004B3 | (0b1 << 22) | (0b1 << 21))
        else:
            # fractional mode
            # Disable charge cancellation and set antibacklash to 6nS (0b0)
            reg_03 = '{:06X}'.format(0x000004B3)
        cmd = 'W00,{},{},{}:H{}:L{}:'.format(reg_03[0:2], reg_03[2:4], reg_03[4:6],
                                             self.le, self.le)
        return cmd

    def config_reg2(self, n_frac) -> str:
        #
        # 0001_1000_0000_0000_0100_1110_0100_0010
        # |\/\__/|| \__________/|\___/| |||| |\_/
        # ||  |  ||       |     |  |  | |||| | |
        # ||  |  ||       |     |  |  | |||| | Reg_02
        # ||  |  ||       |     |  |  | |||| counter reset
        # ||  |  ||       |     |  |  | |||cp three state
        # ||  |  ||       |     |  |  | ||power down
        # ||  |  ||       |     |  |  | |pd polarity
        # ||  |  ||       |     |  |  | ldp
        # ||  |  ||       |     |  |  ldf
        # ||  |  ||       |     |  cp current setting
        # ||  |  ||       |     double buffer r4[22:20]
        # ||  |  ||       R counter
        # ||  |  |reference divide-by-2
        # ||  |  reference doubler
        # ||  muxout: digital lock detect
        # |noise mode
        # reserved

        if n_frac == 0:
            # Integer mode
            reg = 0x18000F42
        else:
            # Fractional mode
            reg = 0x18000E42

        # Reference doubler and div-by-2 should be mutually exclusive
        if self.ref_doubler == 0:
            reg_02 = '{:08X}'.format(reg |
                                     (self.pwr_down << 5) |
                                     (self.refdiv << 14) |
                                     (self.ref_divby2 << 24))
        else:
            reg_02 = '{:08X}'.format(reg |
                                     (self.pwr_down << 5) |
                                     (self.refdiv << 14) |
                                     (self.ref_doubler << 25))
        cmd = 'W{},{},{},{}:H{}:L{}:'.format(reg_02[0:2], reg_02[2:4],
                                             reg_02[4:6], reg_02[6:8],
                                             self.le, self.le)
        return cmd

    def config_reg1(self, n_mod: int) -> str:
        #
        # 0000_0000_0000_0000_0000_0000_0000_0001
        # \_/| |\_____________/\_____________/\_/
        #  | | |       |              |        |
        #  | | |       |              |        Reg_01
        #  | | |       |              12-bit modulus
        #  | | |       12-bit phase value
        #  | | prescaler
        #  | phase adjust
        #  reserved

        reg_01 = '{:08X}'.format(0x00008001 |
                                 (n_mod << 3) |
                                 (int(self.prescaler) << 27))
        cmd = 'W{},{},{},{}:H{}:L{}:'.format(reg_01[0:2], reg_01[2:4],
                                             reg_01[4:6], reg_01[6:8],
                                             self.le, self.le)
        return cmd

    def config_reg0(self, n_int: int, n_frac: int) -> str:
        #
        # 0000_0000_0000_0000_0000_0000_0000_0000
        #  \__________________/\_____________/\_/
        #           |                 |        |
        #           |                 |        Reg_00
        #           |                 12-bit fractional value
        #           16-bit integer value

        reg_00 = '{:08X}'.format(0x00000000 |
                                 (n_frac << 3) |
                                 (n_int << 15))
        cmd = 'W{},{},{},{}:H{}:L{}:'.format(reg_00[0:2], reg_00[2:4],
                                             reg_00[4:6], reg_00[6:8],
                                             self.le, self.le)
        return cmd

    def config_pwrdown(self) -> str:
        cmd = self.config_reg2(self.n_frac)
        return cmd

    def configure(self, fout: float) -> str:
        """
        """
        self.divider_values(fout)
        cmd = self.config_reg4(self.div_select)
        cmd += self.config_reg3(self.n_frac)
        cmd += self.config_reg2(self.n_frac)
        cmd += self.config_reg1(self.n_mod)
        cmd += self.config_reg0(self.n_int, self.n_frac)
        cmd += self.config_reg5()
        return cmd

    def device_initialize(self, fout: float) -> str:
        """Configure the PLO after power up.

        :param fout: The initial output frequency in MHz.
        :type fout: float

        :return: The command string required to effect the
            device initialization.

        """
        self.divider_values(fout)
        cmd = self.config_reg5()
        cmd += self.config_reg4(self.div_select)
        cmd += self.config_reg3(self.n_frac)
        cmd += self.config_reg2(self.n_frac)
        cmd += self.config_reg1(self.n_mod)
        cmd += self.config_reg0(self.n_int, self.n_frac)
        cmd += self.config_reg5()
        return cmd

        
