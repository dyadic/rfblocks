# `HMC833Controller` - Control for the HMC833 wideband PLO
#
#    Copyright (C) 2021 Dyadic Pty Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import (
    Dict, Optional
)
import serial

from PyQt5.QtCore import (
    pyqtSignal, QObject
)

from rfblocks import (
    adf4351, write_cmd, query_cmd
)


class ADF4351Controller(QObject):
    """Higher level control for the ADF4351 phase locked oscillator.
    """

    DEFAULT_PLO_FREQ: float = 1000.0
    DEFAULT_REF_FREQ: float = 25.0

    DEFAULT_DEVICE_CONFIG: Dict = {
        'freq': DEFAULT_PLO_FREQ,
        'ref_freq': DEFAULT_REF_FREQ,
        'pwrgain': adf4351.OutputPowerGain.MAXGAIN_MINUS_9DB,
        'pwr_down': False
    }

    freq_changed = pyqtSignal(float)
    ref_freq_changed = pyqtSignal(float)
    pwrgain_changed = pyqtSignal(int)
    pwr_down_changed = pyqtSignal(bool)
    lock_status_changed = pyqtSignal(bool)

    def __init__(self,
                 controller_id: str,
                 device: adf4351,
                 config: Optional[Dict] = None) -> None:
        """
        :param controller_id: The controller name.
        :type controller_id: str
        :param device: An instance of :py:class:`rfblocks.adf4351`.
        :type device: :py:class:`rfblocks.adf4351`
        :param config: Initial configuration for the ADF4351 PLO evaluation
            board.  If this is None the default configuration will be used.
            See :py:attr:`ADF4351Controller.DEFAULT_DEVICE_CONFIG` for a
            brief description of the structure for :py:`config`.

        :py:`ADF4351Controller` maintains the state configuration for an
        ADF4351 PLO eval. board.  The following signals are defined:

            - :py:`freq_changed(float)`
            - :py:`ref_freq_changed(float)`
            - :py:`pwrgain_changed(hmc833.OutputPowerGain)`
            - :py:`pwr_down_changed(bool)`
            - :py:`lock_status_changed(bool)`
        """
        super().__init__()
        self._controller_id: str = controller_id
        self._adf4351: adf4351 = device
        self._initial_config: Dict = config
        if self._initial_config is None:
            self._initial_config = {**ADF4351Controller.DEFAULT_DEVICE_CONFIG}
        self._freq: float = self._initial_config['freq']
        self._adf4351.pwr_gain: adf4351.OutputPowerGain = \
            self._initial_config['pwrgain']
        self._adf4351.pwr_down: bool = self._initial_config['pwr_down']
        self._lock_status: bool = False

    @property
    def plo(self) -> adf4351:
        return self._adf4351

    @property
    def freq(self) -> float:
        """Returns the current output frequency (in MHz)
        """
        return self._freq

    @freq.setter
    def freq(self, f: float) -> None:
        """Set the current output frequency.

        :param f: The new output frequency (in MHz)
        :type f: float

        Note that this will set the value of the :py:attr:`freq`
        property only.  Updating the PLO hardware should be
        done separately.  See, for example,
        :py:meth:`configure_freq`.
        """
        if f != self._freq:
            self._freq = f
            self.freq_changed.emit(self._freq)

    @property
    def pwr_gain(self) -> adf4351.OutputPowerGain:
        """The PLO current output power gain setting.
        """
        return self.plo.pwr_gain

    @pwr_gain.setter
    def pwr_gain(self, gain: adf4351.OutputPowerGain) -> None:
        """Set the PLO current output power gain.

        :param gain: The VCO output power gain setting.
        :type gain: :py:class:`rfblocks.adf4351.OutputPowerGain`

        Note that this will set the value of the :py:attr:`pwr_gain`
        property only.  Updating the PLO hardware should be
        done separately.  See, for example,
        :py:meth:`configure_gains`.
        """
        if gain != self.plo.pwr_gain:
            self.plo.pwr_gain = gain
            self.pwrgain_changed.emit(gain)
    
