# `hmc1031` - Encapsulates control of the HMC1031 clock generator.
#
#    Copyright (C) 2022 Dyadic Pty Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import (
    Optional
)
from enum import IntFlag


class hmc1031:
    """Encapsulates control of the HMC1031 clock generator.
    """

    class ReferenceSource(IntFlag):
        """An `enum` containing the possible settings for the reference
        source. (``INTERNAL``, ``EXTERNAL``)
        """
        INTERNAL = 0b00
        EXTERNAL = 0b01

    def __init__(self,
                 ref: Optional[str] = None,
                 locked: Optional[str] = None,
                 refsrc: ReferenceSource = ReferenceSource.INTERNAL):
        self.ref = ref.upper() if ref else ref
        self.locked = locked.upper() if locked else locked
        self.refsrc = refsrc

    def __str__(self):
        """
        """
        return 'HMC1031: refsrc: {}, locked: {}'.format(
            self.refsrc, self.locked)

    def __repr__(self):
        return self.__str__()

    def pin_config(self):
        """Initialize pin configuration.

        :return: A string specifying the commands required to initialize the
            connected controller pins.
        """
        cmd = ''
        if self.refsrc:
            cmd += 'O{}:L{}:'.format(self.refsrc, self.refsrc)
        if self.locked:
            cmd += 'I{}'.format(self.locked)
        return cmd

    def chip_reset(self):
        """Reset the chip internal logic to default states.

        :return: A string containing the controller commands required to reset
            the chip.
        """
        return ""

    def config_refsrc(self) -> str:
        """Configure the reference source.

        :returns: The command string required to set the reference
            source switch.
        """
        cmd = ""
        if self.ref is not None:
            if self.refsrc == hmc1031.ReferenceSource.INTERNAL:
                cmd = "H{}:".format(self.ref)
            else:
                cmd = "L{}:".format(self.ref)
        return cmd

    def check_is_locked(self) -> str:
        """
        """
        if self.locked:
            return 'P{}:'.format(self.locked)
        else:
            return ''
