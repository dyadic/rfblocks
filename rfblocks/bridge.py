import os
import sys
from time import sleep
from dataclasses import dataclass
from typing import Any
import signal
import logging
import argparse
from argparse import ArgumentParser
from collections import Counter
from functools import partial
import json
import asyncio
from serial.serialutil import SerialException
from serial.tools.list_ports import comports
import aioserial
import rpyc
from rpyc.utils.server import ThreadedServer
from qasync import (
    QEventLoop, QThreadExecutor, asyncSlot
)
from PyQt5.QtCore import (
    Qt, QObject, QCoreApplication, QThread, QTimer,
    pyqtSignal, pyqtSlot
)


DEFAULT_BAUDRATE = 1500000

logging.basicConfig(
    level=logging.DEBUG,
    format='%(name)s: %(message)s',
    stream=sys.stderr,
)
log = logging.getLogger('main')


def get_logger(sock_stream):
    addr = sock_stream.get_extra_info('peername')
    return logging.getLogger('client_{}_{}'.format(*addr))


class SerialDeviceSpecException(Exception):
    """Raised when there's a problem with a serial port specification."""
    pass


@dataclass
class SerialServer:
    device: str
    baudrate: int
    port: int
    task: Any
    serial_number: str


class BridgeService(rpyc.Service):

    def __init__(self, app):
        super().__init__()
        self._app = app
        self._clients = []

    def reconfig_devices(self):
        self._app.reconfigure.emit()

    @property
    def connected_devices(self):
        return self._app.connected_devices

    def on_connect(self, conn):
        self._clients.append(conn)

    def on_disconnect(self, conn):
        self._clients.remove(conn)


class RPyCServer(QObject):

    finished = pyqtSignal()

    def __init__(self, serviceInst, host, port):
        super().__init__()
        self._serviceInst = serviceInst
        self._host = host
        self._port = port

    def run(self):
        print("Run RPyC ThreadedServer on {}:{}".format(self._host, self._port))
        self._server = ThreadedServer(
            self._serviceInst,
            hostname = self._host,
            port = self._port,
            auto_register = True,
            protocol_config = {
                'allow_all_attrs': True,
                'allow_setattr': True,
                'allow_pickle': True})
        self._server.start()
        self.finished.emit()


class BridgeApp(QCoreApplication):

    STARTING = 0
    RUNNING = 1
    RECONFIGURING = 2
    EXITING = 3
    
    reconfigure = pyqtSignal()
    device_added = pyqtSignal(object)
    device_removed = pyqtSignal(object)
    
    def __init__(self, cmdline_args):
        super().__init__([])
        self._state = BridgeApp.STARTING
        self._base_port = cmdline_args.ipport
        self._rpyc_port = cmdline_args.rpyc_port
        self._server_ip = cmdline_args.ipaddr
        self._device_specs = cmdline_args.devices
        self._known_device_file = cmdline_args.config
        self._connected_devices = {}
        self.configured_devices = []
        self.reconfigure.connect(self.reconfig_devices)
        self.device_added.connect(self.add_configured_device)
        self.device_removed.connect(self.remove_configured_device)

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, s):
        self._state = s

    @property
    def server_ip(self):
        return self._server_ip

    @property
    def rpyc_port(self):
        return self._rpyc_port

    @property
    def base_port(self):
        return self._base_port

    @property
    def device_specs(self):
        return self._device_specs

    @property
    def known_device_file(self):
        return self._known_device_file

    @property
    def connected_devices(self):
        return self._connected_devices

    @pyqtSlot(object)
    def add_configured_device(self, serial_srv):
        self._connected_devices[serial_srv.serial_number] = (
            serial_srv.device, serial_srv.baudrate, serial_srv.port
        )

    @pyqtSlot(object)
    def remove_configured_device(self, serial_srv):
        try:
            self._connected_devices.pop(serial_srv.serial_number)
        except KeyError:
            pass

    def reconfig_devices(self):
        self.state = BridgeApp.RECONFIGURING
        loop = asyncio.get_event_loop()
        loop.stop()
    
    def config_devices(self):
        self.configured_devices = []
        self.serial_devices = self.config_serial_devices()
        loop = asyncio.get_event_loop()
        for dev_spec in self.serial_devices:
            serial_dev, baudrate, server_port, dev_serial_number = dev_spec
            retry_serial = False
            try:
                aioserial_instance = aioserial.AioSerial(
                    port=serial_dev, baudrate=baudrate)
            except SerialException as se:
                print(f'{se=}')
                retry_serial = True

            if retry_serial is True:
                print('Retrying...')
                sleep(1)
                aioserial_instance = aioserial.AioSerial(
                    port=serial_dev, baudrate=baudrate)
                
            coro = asyncio.start_server(
                partial(handle_requests, aioserial_instance),
                self.server_ip, server_port, loop=loop)
            server = loop.run_until_complete(coro)
            log.info('{}: {}'.format(serial_dev, server_port))
            serial_srv = SerialServer(serial_dev, baudrate,
                                      server_port, server,
                                      dev_serial_number)
            self.configured_devices.append(serial_srv)
            self.device_added.emit(serial_srv)
        
    def config_serial_devices(self):
        serial_devices = self.config_known_devices()
        for index, dev in enumerate(self.device_specs):
            device, baudrate = dev[0], int(dev[1])
            if len(dev[2]) == 0:
                port = self.base_port + index
            else:
                port = int(dev[2])
            serial_devices.append((device, baudrate, port))

            # Check for duplicate serial devices or ports
            port_dupl = [p for p, cnt in Counter(
                [dev[2] for dev in serial_devices]).items() if cnt > 1]
            dev_dupl = [d for d, cnt in Counter(
                [dev[0] for dev in serial_devices]).items() if cnt > 1]
            if len(port_dupl):
                raise SerialDeviceSpecException(
                    "Duplicate port specification: {}".format(port_dupl))
            if len(dev_dupl):
                raise SerialDeviceSpecException(
                    "Duplicate serial device specification: {}".format(dev_dupl))
        return serial_devices

    def config_known_devices(self):
        with open(self.known_device_file) as fd:
            known_devices = json.load(fd)

        device_list = []
        for port_info in comports():
            try:
                known_dev = known_devices[port_info.serial_number]
                serial_dev = port_info.device
                server_port = known_dev['port']
                baudrate = known_dev['baudrate']
                if baudrate is None:
                    baudrate = DEFAULT_BAUDRATE
                device_list.append(
                    (serial_dev, baudrate, server_port, port_info.serial_number))
            except KeyError:
                pass
        return device_list

    def close_devices(self):
        for serial_srv in self.configured_devices:
            log.info('Closing: {}'.format(serial_srv.device))
            serial_srv.task.close()
            self.device_removed.emit(serial_srv)
        self.configured_devices = []
            

def list_available_serial_ports():
    """
    """
    for port_info in comports():
        print(port_info.device)
        if port_info.description not in ['n/a']:
            descr = port_info.description
        else:
            descr = os.path.splitext(port_info.device)[1][1:]
        print("  Description:   {}".format(descr))
        print("  Hw Id:         {}".format(port_info.hwid))
        print("  Serial number: {}".format(port_info.serial_number))


async def read_serial_response(aioserial_instance, writer):
    log = get_logger(writer)
    resp = []
    while True:
        bytes = await aioserial_instance.read_async()
        resp.append(bytes)
        if b'.' in resp:
            break
    log.debug("send: %r" % b''.join(resp))
    writer.write(b''.join(resp))
    await writer.drain()


async def handle_requests(aioserial_instance, reader, writer):
    """This is executed by asyncio server instances created when
    BridgeApp.config_devices is run.
    """
    log = get_logger(writer)
    while True:
        # Read single line request from client
        try:
            data = await reader.readuntil(separator=b'\r')
        except ConnectionResetError:
            log.info("Connection reset by peer.")
            writer.close()
            return
        except asyncio.IncompleteReadError as ire:
            if reader.at_eof() is not True:
                log.info("Incomplete read: '{}'".format(ire))
            else:
                log.info("  EOF: {}".format(reader.at_eof()))
            data = None

        if data:
            request = data.decode()
            log.debug("received {!r}".format(request))

            # Send the received request to the serial device and wait for
            # a response. On receiving the response send it on to the
            # requestor (see read_serial_response).
            await asyncio.gather(
                read_serial_response(aioserial_instance, writer),
                aioserial_instance.write_async(
                    bytes('{}'.format(request), "utf8")))
            data = None
        else:
            log.info("close the client socket")
            writer.close()
            await writer.wait_closed()
            return


def main(app):
    
    loop = QEventLoop(app)
    loop.set_default_executor(QThreadExecutor(1))
    asyncio.set_event_loop(loop)

    app.config_devices()
    
    server_thread = QThread()
    server = RPyCServer(BridgeService(app),
                        app.server_ip,
                        app.rpyc_port)
    server.moveToThread(server_thread)
    server_thread.started.connect(server.run)
    server.finished.connect(server_thread.quit)
    server.finished.connect(server.deleteLater)
    server_thread.finished.connect(server_thread.deleteLater)
    server_thread.start()

    # Serve requests until Ctrl+C is pressed
    try:
        while True:
            if app.state == BridgeApp.STARTING:
                app.state = BridgeApp.RUNNING
                # Note that this loop will be stopped when a 'reconfigure'
                # signal is received
                loop.run_forever()
            elif app.state == BridgeApp.RECONFIGURING:
                # This is executed after a 'reconfigure' signal is received
                app.close_devices()
                app.config_devices()
                app.state = BridgeApp.RUNNING
                loop.run_forever()
            elif app.state == BridgeApp.EXITING:
                break
    except KeyboardInterrupt:
        pass

    loop.close()


def serial_dev_spec(s):
    """Parse serial device specification

    Parameters
    ----------
    s - string
        Command line specification for a serial device in the form
        '<device>:<baudrate>:<port>'. Both <baudrate> and <port>
        are optional.

        If <baudrate> is not specified a default value of '1500000'
        will be used.

        If <port> is not specified a zero length string will be used

        Examples:

            '/dev/tty.AMA0'
                -> ('/dev/tty.AMA0', '1500000', '')
            '/dev/cu.usbmodem14101:38400'
                -> ('/dev/cu.usbmodem14101, '38400', '')
            '/dev/tty.AMA0::7001'
                -> ('/dev/tty.AMA0', '1500000', '7001')
            '/dev/tty.usbmodem621:115200:7002'
                -> ('/dev/tty.usbmodem621', '115200', '7002')

            '/dev/tty.AMA0 /dev/tty.AMA1'
                -> [ ('/dev/tty.AMA0', '1500000', ''),
                     ('/dev/tty.AMA1', '1500000', '') ]

    """
    serial_spec = s.split(':')
    if len(serial_spec) == 1:
        serial_spec += ['1500000', '']
    elif len(serial_spec) == 2:
        serial_spec += ['']
    if len(serial_spec[0]) == 0:
        raise argparse.ArgumentTypeError(
            "serial device spec must have a non-empty device string")
    if len(serial_spec[1]) == 0:
        serial_spec[1] = '1500000'
    return serial_spec


if __name__ == '__main__':
    default_server_ip = "127.0.0.1"
    default_server_port = 7000
    default_rpyc_port = 18901
    default_config = '/opt/etc/bridge.json'

    # This ensures that Cntl-C will work as expected:
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    parser = ArgumentParser(description='rfblocks TCP to serial bridge.')

    parser.add_argument(
        "-l", "--listports", action='store_true',
        help="List available serial ports")
    parser.add_argument(
        "-I", "--ipaddr", default=default_server_ip,
        help="Server IP address")
    parser.add_argument(
        "-P", "--ipport", default=default_server_port,
        help="Base server IP port")
    parser.add_argument(
        "-p", "--rpyc_port", default=default_rpyc_port,
        help="RPyC server port"
    )
    parser.add_argument(
        "-C", "--config", default=default_config,
        help="File containing descriptions of known devices"
    )
    parser.add_argument(
        "devices", nargs='*', type=serial_dev_spec,
        help="Zero or more serial device specifications of the"
        " form '<device>:<baudrate>:<port>' (<baudrate> and <port>"
        " are optional")

    args = parser.parse_args()

    if args.listports:
        list_available_serial_ports()
        sys.exit(0)

    logging.getLogger().setLevel(logging.WARNING)

    app = BridgeApp(args)
    main(app)
