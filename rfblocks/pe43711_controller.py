# `PE43711Controller` - Control for the PE43711 step attenuator board.
#
#    Copyright (C) 2021 Dyadic Pty Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import (
    Optional, Callable, Dict
)

from PyQt5.QtCore import (
    pyqtSignal, QObject
)

import serial

from rfblocks import (
    pe43711, write_cmd
)


class PE43711Controller(QObject):
    """Higher level control for the PE43711 step attenuator board

    Documentation for the PE43711 step attenuator rfblocks module
    can be found here: `A PE43711 Step Attenuator <../boards/PE43711-Step-Attenuator.html>`_

    """

    attenuation_changed = pyqtSignal(object)

    def __init__(self,
                 controller_id: str,
                 device: pe43711) -> None:
        """
        :param controller_id: The controller name
        :type controller_id: str
        :param device: :py:class:`rfblocks.pe43711`.
        :type device: :py:class:`rfblocks.pe43711`
        """
        super().__init__()
        self._controller_id: str = controller_id
        self._pe43711: pe43711 = device
        self._atten: float = pe43711.MAX_ATTENUATION
        # The actual device attenuation setting.
        # The PE43711 powers up with the max. attenuation
        # setting.
        self._dev_atten: float = pe43711.MAX_ATTENUATION

    def __str__(self) -> str:
        s = f'{self.__class__.__name__}: attenuation: {self.attenuation}'
        return s

    def __repr__(self) -> str:
        return "{}({!r})".format(self.__class__.__name__, vars(self))

    def dump_config(self) -> Dict:
        """Return the current configuration for this attenuator.

        :return: A dictionary containing the current attenuator
                 configuration:

            :atten: attenuation in dB
        """
        config = {
            'atten': self.attenuation
        }
        return config

    def load_config(self, config: Dict) -> None:
        """Set the current configuration for this attenuator.

        :param config: A dictionary containing the attenuator configuration
            to be set.
        :type config: Dict

        Note that in order to update the attenuator hardware
        :py:meth:`set_attenuation` should be called.
        """
        self.attenuation = config['atten']

    def initialize(self, ser: serial.Serial):
        write_cmd(ser, self._pe43711.pin_config())

    @property
    def attenuation(self) -> float:
        return self._atten

    @attenuation.setter
    def attenuation(self, attn_db: float) -> None:
        """The controller keeps track of the current device attenuation
        setting.  If the requested attenuation as returned by
        :py:`self.attenuation` is different then the device will
        be commanded to update it's setting and an *attenuation_changed*
        signal is emitted.
        """
        if attn_db != self._atten:
            self._atten = attn_db
            self.attenuation_changed.emit(self._atten)

    def configure(self, ser: serial.Serial) -> None:
        """Set the current device attenuation.

        :raises: :py:exc:`AttenuationRangeException`
            If the specified attenuation is out of range.
        """
        attn_word = self._pe43711.atten_word(self.attenuation)
        cmd = self._pe43711.config_atten_word(attn_word)
        write_cmd(ser, cmd)
        self._dev_atten = self.attenuation
