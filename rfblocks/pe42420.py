# `pe42420` - Encapsulates control of the PE42420 RF switch.
#
#    Copyright (C) 2022 Dyadic Pty Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from enum import IntFlag


class pe42420:
    """Encapsulates control of the PE42420 RF switch.

    Documentation for the SPDT RF switches rfblocks module which uses the PE42420 
    can be found here: `SPDT RF Switches <../boards/SPDT-RF-Switches.html>`_

    """

    class State(IntFlag):
        """An `enum` containing the possible states for the switch.
        (``pe42420.State.OFF``, ``pe42420.State.RF1``, ``pe42420.State.RF2``)
        """
        OFF = 0b00  # Set the switch to mute both channels.
        RF1 = 0b01  # Connect RFC to RF1
        RF2 = 0b10  # Connect RFC to RF2

    def __init__(
            self,
            c1: str = None,
            c2: str = None) -> None:
        """
        :param c1:  The PE42420 `CTRL1` controller pin.
        :type c1: str
        :param c2: The PE42420 `CTRL2` controller pin.
        :type c2: str
        """
        self.c1 = c1.upper() if c1 else c1
        self.c2 = c2.upper() if c2 else c2

    def __str__(self):
        return 'PE42420: c1: {}, c2: {}'.format(self.c1, self.c2)

    def __repr__(self):
        return self.__str__()

    def pin_config(self) -> str:
        """Initialize controller pin configuration.

        :return: A string specifying the commands required to initialize the
            connected controller pins.
        """
        cmd = ''
        if self.c1:
            cmd += 'O{}:L{}:'.format(self.c1, self.c1)
        if self.c2:
            cmd += 'O{}:L{}:'.format(self.c2, self.c2)
        return cmd

    def chip_reset(self) -> str:
        """Reset the chip internal logic to default states.

        :return: A string containing the controller commands required to reset
            the chip.
        """
        return ""

    def config_state(self, state: State) -> str:
        """Configure the switch state

        :param state: Set the switch to this state
        :type state: State

        :return: A string containing the controller commands required to
            set the switch hardware configuration.
        """
        cmd = ''
        if state is pe42420.State.OFF:
            cmd = 'L{}:L{}:'.format(self.c1, self.c2)
        elif state is pe42420.State.RF1:
            cmd = 'H{}:L{}:'.format(self.c1, self.c2)
        elif state is pe42420.State.RF2:
            cmd = 'L{}:H{}:'.format(self.c1, self.c2)
        return cmd
