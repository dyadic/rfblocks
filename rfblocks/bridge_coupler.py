# `BridgeCoupler` - Encapsulates characteristics for the resistive bridge
#                   directional coupler.
#
#    Copyright (C) 2020 Dyadic Pty Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import (
    Tuple, Optional, Callable
)

from .hmc833 import (
    FrequencyRangeException
)


class BridgeCoupler(object):
    """Encapsulates characteristics of the resistive bridge directional
       coupler.
    """

    MIN_FREQ = 0.5
    MAX_FREQ = 6000.0

    @classmethod
    def default_insertion_loss(cls, freq: float) -> float:
        """Calculate default coupler insertion loss.

        :param freq: The frequency (in MHz) to compute the insertion
            loss.
        :type freq: float

        :return: The default insertion loss (in dB) at the specified
             frequency
        """
        if freq < BridgeCoupler.MIN_FREQ or freq > BridgeCoupler.MAX_FREQ:
            raise FrequencyRangeException
        DEFAULT_LOSS_INTERCEPT = -1.35
        DEFAULT_LOSS_SLOPE = -2.33e-4
        return DEFAULT_LOSS_INTERCEPT + (DEFAULT_LOSS_SLOPE * freq)

    @classmethod
    def default_coupling(cls, freq: float) -> float:
        """Calculate default coupling.

        :param freq: The frequency (in MHz) to compute the coupling.
        :type freq: float

        :return: The default coupling (in dB) at the specified frequency.
        """
        if freq < BridgeCoupler.MIN_FREQ or freq > BridgeCoupler.MAX_FREQ:
            raise FrequencyRangeException
        COEFFS = [-2.68e-21, 3.39e-17, -1.18e-13,
                  1.61e-11, 3.47e-07, -2.40e-04, -16.0]
        return (((((COEFFS[0]*freq + COEFFS[1])*freq +
                   COEFFS[2])*freq + COEFFS[3])*freq +
                 COEFFS[4])*freq + COEFFS[5])*freq + COEFFS[6]

    def __init__(self):
        """Create an instance of ``BridgeCoupler``
        """
        self._loss_fn = BridgeCoupler.default_insertion_loss
        self._coupling_fn = BridgeCoupler.default_coupling

    def insertion_loss(self, freq: float) -> float:
        """Return the coupler insertion loss (in dB) at the specified
        frequency.

        :param freq: frequency in MHz
        :type freq: float
        """
        return self._loss_fn(freq)

    def set_insertion_loss_fn(self,
                              fn: Optional[Callable[[float], float]]) -> None:
        """Set a callable to use when calculating the insertion loss.

        :param fn: A callable which takes a single float parameter specifying
            the frequency in MHz and returning the insertion loss in dB.
        """
        self._loss_fn = fn

    def coupling(self, freq: float) -> float:
        """Return the coupler coupling (in dB) at the specified frequency.

        :param freq: frequency in MHz
        :type freq: float
        """
        return self._coupling_fn(freq)

    def set_coupling_fn(self,
                        fn: Optional[Callable[[float], float]]) -> None:
        """Set a callable to use when calculating the coupling.

        :param fn: A callable which takes a single float parameter specifying
            the frequency in MHz and returning the coupling in dB.
        """
        self._coupling_fn = fn
