# `RPDacController` - Control for the RedPitaya DAC
#
#    Copyright (C) 2023 Dyadic Pty Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import (
    Optional, Dict, List
)

from PyQt5.QtCore import (
    pyqtSignal, QObject
)

from math import sqrt, log10
import numpy as np
from scipy import interpolate


class RPDacController(QObject):
    """Control for the RedPitaya DAC.
    """

    MAX_LEVEL: int = (1 << 15) - 1

    DEFAULT_BOARD_MODEL: str = '122-16'
    DEFAULT_DAC_FREQ: float = 10.0
    DEFAULT_DAC_LEVEL: int = 8192

    CAL_DATA = {
        "122-16": {
            0.0: [0.009629220474775657, 0.26452767623699436],
            2.5: [0.009629220474775657, 0.26452767623699436],
            5.25: [0.009838792062192736, -0.00641321932820027],
            8.0: [0.009913859330833774, 0.06667589165244336],
            10.75: [0.009798162118345166, 0.13541222207548154],
            13.5: [0.009753842082298347, -0.0012354794405224911],
            16.25: [0.009655852527926667, -0.012831955312503807],
            19.0: [0.00959316842219915, 0.07166632891996105],
            21.75: [0.009469854157923674, -0.050455721397679065],
            24.5: [0.009264505925070665, 0.018847684617459004],
            27.25: [0.009133004441556958, 0.007489091329943798],
            30.0: [0.008986358021089158, 0.029385943125152636],
            32.75: [0.008819169323722731, -0.013672307451767729],
            35.5: [0.00862810112734691, -0.07241805905789822],
            38.25: [0.008404260863890012, 0.04110593324094347],
            41.0: [0.00817116849077093, 0.06495578997797702],
            43.75: [0.007933535736433184, 0.00908005536884049],
            46.5: [0.007693021701936975, -0.018016039067610956],
            49.25: [0.007441507449868712, -0.04448076517010513],
            52.0: [0.007177954679948972, 0.04204461430694595],
            54.75: [0.006915386778077539, -0.04971109265954342],
            55.0: [0.006915386778077539, -0.04971109265954342]
        }
    }

    freq_changed = pyqtSignal(float)
    level_changed = pyqtSignal(float)

    @classmethod
    def board_models(cls) -> List:
        """Returns a list containing the board model names for which
        calibration data is available.

        :classmethod:
        """
        return list(RPDacController.CAL_DATA.keys())

    def __init__(self,
                 controller_id: str,
                 chan_id: int,
                 adcdac_service,
                 board_model: str = DEFAULT_BOARD_MODEL) -> None:
        """
        :param controller_id:
        :type controller_id: str
        :param chan_id:
        :type chan_id: int
        :param board_model:
        :type board_model: str
        """
        super().__init__()
        self._chan_id = chan_id
        self._adcdac_service = adcdac_service
        self._freq = RPDacController.DEFAULT_DAC_FREQ
        self._level = RPDacController.DEFAULT_DAC_LEVEL
        self._board_model = board_model
        self.set_cal_data(self.cal_data)
        self._ch = None

    def allocate_chan(self) -> None:
        """
        """
        self._ch = self._adcdac_service.root.allocate_dac_channel(self.chan_id)

    def free_chan(self) -> None:
        """
        """
        if self._ch is not None:
            self._adcdac_service.root.free_dac_channel(self.chan_id)
            self._ch = None

    def configure(self) -> None:
        """
        """
        if self._ch is None:
            self.allocate_chan()
        self._ch.freq = self.freq
        self._ch.ampl = self.level

    @property
    def board_model(self) -> str:
        """
        """
        return self._board_model

    @property
    def cal_data(self) -> Dict[float, List[float]]:
        """The calibration data for the RedPitaya board associated with this
        RPDacController instance.

        The format for the data is:

        .. code:: python

            {
                 freq-0: [slope-0, intercept-0],
                 freq-1: [slope-1, intercept-1],
                 ...
                 freq-n [slope-n, intercept-n]
            }

        where the frequencies run from 0 to 55MHz.

        The slope and intercept data are derived from calibration
        measurements using the procedure documented in
        `Calibration Procedure <https://www.rfblocks.org/boards/AD9913-DDS.html#calibration-procedure>`_.

        Note that calibration measurements are made down to
        2.5MHz so the values measured at 2.5MHz are also used for 0MHz.
        This allows slope and intercept numbers to be interpolated
        for f < 5MHz.
        Likewise, measurements are made up to 54.75MHz and the slope and
        intercept measured at 54.75MHz are also used for 55MHz.
        """
        return RPDacController.CAL_DATA[self.board_model]
    
    @property
    def chan_id(self) -> int:
        """
        """
        return self._chan_id

    @property
    def freq(self) -> float:
        """The DAC channel output frequency in MHz.

        The :py:`freq_changed(float)` signal is emitted if the :py:`freq`
        value changes.

        :type: float

        Note that :py:meth:`configure` must be invoked in order to
        update the DAC hardware

        >>> adcdac = rpyc.connect("192.168.0.155", 18900)
        >>> adcdac.root.initialize()
        True
        >>> ctl = RPDacController('dac_ctl_1', 0, adcdac)
        >>> ctl.freq
        10.0
        >>> ctl.freq = 25.0
        >>> ctl.freq
        25

        """
        return self._freq

    @freq.setter
    def freq(self, f: float) -> None:
        if self._freq != f:
            self._freq = f
            self.freq_changed.emit(f)

    @property
    def level(self) -> float:
        """The DAC channel output level in DAC units (DAC code).

        The RedPitaya Zynq PL and DAC hardware uses a 10-bit DAC so the level
        range is 0 to 1023.
        This emits the :py:`level_changed(float)` signal if the :py:`level`
        value changes.

        :type: float

        Note that :py:meth:`configure` must be invoked in order to
        update the DAC hardware

        >>> adcdac = rpyc.connect("192.168.0.155", 18900)
        >>> adcdac.root.initialize()
        True
        >>> ctl = RPDacController('dac_ctl_1', 0, adcdac)
        >>> ctl.level
        8192
        >>> ctl.level = 8192*2
        >>> ctl.level
        16384

        """
        return self._level

    @level.setter
    def level(self, lvl: float) -> None:
        if self._level != round(lvl):
            self._level = round(lvl)
            self.level_changed.emit(lvl)

    @property
    def level_range(self) -> List[float]:
        """Return the range of the level setting.

        The range is returned as a two element list: [min_level, max_level]
        """
        return [1, RPDacController.MAX_LEVEL]

    @property
    def millivolts(self) -> float:
        """The DAC channel output level in millivolts.

        This emits the :py:`level_changed(float)` signal if the :py:`level`
        value is changed.

        :type: float

        Note that :py:meth:`configure` must be invoked in order to
        update the DAC hardware

        >>> adcdac = rpyc.connect("192.168.0.155", 18900)
        >>> adcdac.root.initialize()
        True
        >>> ctl = RPDacController('dac_ctl_1', 0, adcdac)
        >>> ctl.level = 8192*2
        >>> ctl.level
        16384
        >>> f'{ctl.millivolts:.2f}'
        '151.41'
        >>> ctl.millivolts = 300
        >>> ctl.level
        32464
        >>> ctl.level_range
        [1, 32767]
        >>> ctl.millivolts_range
        [0.016782485837910786, 302.80341011257497]
        """
        return self.level_to_millivolts(self.level)

    @millivolts.setter
    def millivolts(self, mv: float) -> None:
        self.level = self.millivolts_to_level(mv)

    @property
    def millivolts_range(self) -> List[float]:
        """Return the range of the millivolts setting.

        The range is returned as a two element list:
        [min_millivolts, max_millivolts]
        """
        return [self.level_to_millivolts(0),
                self.level_to_millivolts(RPDacController.MAX_LEVEL)]

    @property
    def dbm(self) -> float:
        """The DAC channel output level in dBm.

        This emits the :py:`level_changed(float)` signal if the :py:`level`
        value is changed.

        :type: float

        Note that :py:meth:`configure` must be invoked in order to
        update the DAC hardware

        >>> adcdac = rpyc.connect("192.168.0.155", 18900)
        >>> adcdac.root.initialize()
        True
        >>> ctl = RPDacController('dac_ctl_1', 0)
        >>> ctl.millivolts = 100
        >>> f'{ctl.dbm:.2f}'
        '-6.99'
        >>> ctl.dbm = -10.0
        >>> ctl.level
        7650
        >>> ctl.dbm_range
        [-82.49257425590349, 2.633515192535693]

        """
        return self.level_to_dbm(self.level)

    @dbm.setter
    def dbm(self, d: float):
        self.level = self.dbm_to_level(d)

    @property
    def dbm_range(self) -> List[float]:
        """Return the range of the dbm (power) setting.

        The range is returned as a two element list:
        [min_dbm, max_dbm]
        """
        return [self.level_to_dbm(0),
                self.level_to_dbm(RPDacController.MAX_LEVEL)]

    def set_cal_data(self, cal_data: Dict[float, List[float]]) -> None:
        """Set the calibration data for the RedPitaya board associated
        with the controller.

        :param cal_data: The new calibration data to be used.
        :type cal_data: Dict[float, List[float]]

        The format for the data is:

        .. code:: python

            {
                freq-0: [slope-0, intercept-0],
                freq-1: [slope-1, intercept-1],
                ...
                freq-n [slope-n, intercept-n]
            }

        where the frequencies run from 0 to 55MHz.

        The slope and intercept data are derived from calibration
        measurements using the procedure documented in
        `Calibration Procedure <https://www.rfblocks.org/boards/AD9913-DDS.html#calibration-procedure>`_.

        Note that calibration measurements are made down to
        2.5MHz so the values measured at 5MHz are also used for 0MHz.
        This allows slope and intercept numbers to be interpolated
        for f < 2.5MHz.

        This will override any default calibration data as loaded during the
        initialization of the controller instance.
        """
        freqs = np.array([float(f) for f in cal_data.keys()])
        slopes = np.array([v[0] for v in cal_data.values()])
        intercepts = np.array([v[1] for v in cal_data.values()])
        self.slope_interp_fn = interpolate.interp1d(freqs, slopes)
        self.intercept_interp_fn = interpolate.interp1d(freqs, intercepts)

    @property
    def slope(self) -> float:
        """Return the slope of the DAC code/output mV relation at the
        currently set output frequency.

        :type: float

        >>> ctl = RPDacController('dac_ctl_1', 0)
        >>> ctl.freq = 20.0
        >>> f'{ctl.slope:.2f}'
        '0.58'

        """
        return float(self.slope_interp_fn(self.freq))

    @property
    def intercept(self) -> float:
        """Return the intercept of the DAC code/output mV relation at the
        currently set output frequency.

        :type: float

        >>> ctl = RPDacController('dac_ctl_1', 0)
        >>> ctl.freq = 20.0
        >>> f'{ctl.intercept:.2f}'
        '1.78'

        """
        return float(self.intercept_interp_fn(self.freq))

    def level_to_millivolts(self, lvl):
        """Return RMS output voltage for a given DAC code.

        >>> ctl = RPDacController('dac_ctl_1', 0)
        >>> ctl.freq = 20.0
        >>> f'{ctl.level_to_millivolts(100.0):.2f}'
        '56.91'
        >>> ctl.freq = 40.0
        >>> f'{ctl.level_to_millivolts(100.0):.2f}'
        '53.97'

        """
        return self.slope * lvl + self.intercept

    def millivolts_to_level(self, mv) -> int:
        """Return the DAC code which produces a given RMS output voltage.

        >>> ctl = RPDacController('dac_ctl_1', 0)
        >>> ctl.freq = 40.0
        >>> f'{ctl.level_to_millivolts(100.0):.2f}'
        '53.97'
        >>> f'{ctl.millivolts_to_level(53.97):.2f}'
        '100.00'

        """
        return round((mv - self.intercept) / self.slope)

    def level_to_dbm(self, lvl) -> float:
        """Return the output power (in dBm) for a given DAC code.

        >>> ctl = RPDacController('dac_ctl_1', 0)
        >>> ctl.freq = 40.0
        >>> f'{ctl.level_to_dbm(100):.2f}'
        '-12.35'
        >>> f'{ctl.level_to_dbm(512):.2f}'
        '1.56'

        """
        mvrms = self.level_to_millivolts(lvl)
        return 10*log10(((mvrms * 1e-3)**2 / 50e-3))

    def dbm_to_level(self, d) -> float:
        """Return the DAC code for a given output power (in dBm).

        >>> ctl = RPDacController('dac_ctl_1', 0)
        >>> ctl.freq = 40.0
        >>> f'{ctl.level_to_dbm(100):.2f}'
        '-12.35'
        >>> f'{ctl.level_to_dbm(512):.2f}'
        '1.56'
        >>> f'{ctl.dbm_to_level(1.56):.2f}'
        '512.00'
        >>> f'{ctl.dbm_to_level(-12.35):.2f}'
        '100.00'

        """
        # Calculate the RMS output voltage (in millivolts)
        mv = sqrt(10**(d/10) * 50e-3) * 1e3
        return self.millivolts_to_level(mv)
