# `PE42420Controller` - Control for the PE42420 RF Switch board.
#
#    Copyright (C) 2022 Dyadic Pty Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import (
    Dict, Optional
)

from PyQt5.QtCore import (
    pyqtSignal, QObject
)

import serial

from rfblocks import (
    pe42420, write_cmd
)


class PE42420Controller(QObject):
    """Higher level control for the PE42420 RF switch board

    Documentation for the SPDT RF switches rfblocks module which uses the PE42420 
    can be found here: `SPDT RF Switches <../boards/SPDT-RF-Switches.html>`_

    """

    state_changed = pyqtSignal(object)

    def __init__(self,
                 controller_id: str,
                 device: pe42420,
                 initial_state: pe42420.State = pe42420.State.OFF) -> None:
        """
        :param controller_id: The controller name
        :type controller_id: str
        :param device: :py:class:`rfblocks.pe42420`.
        :type device: :py:class:`rfblocks.pe42420`
        """
        super().__init__()
        self._controller_id: str = controller_id
        self._pe42420: pe42420 = device
        self._state: pe42420.State = initial_state

    def __str__(self) -> str:
        s = f'{self.__class__.__name__}: state: {self.state}'
        return s

    def __repr__(self) -> str:
        return "{}({!r})".format(self.__class__.__name__, vars(self))

    def initialize(self, ser: serial.Serial) -> None:
        write_cmd(ser, self._pe42420.pin_config())

    def dump_config(self) -> Dict:
        """Return the current switch configuration.

        :return: A dictionary containing the current switch
                 state.
        """
        config = {
            'state': self.state
        }
        return config

    def load_config(self, config: Dict) -> None:
        """Set the current switch configuration.

        :param config: A dictionary containing the switch
            configuration to be set.
        :type config: Dict

        Note that in order to update the switch hardware
        :py:meth:`configure` should be called.
        """
        self.state = config['state']

    @property
    def state(self) -> pe42420.State:
        return self._state

    @state.setter
    def state(self, s: pe42420.State) -> None:
        if s is not self._state:
            self._state = s
            self.state_changed.emit(self._state)

    def configure(self, ser: serial.Serial) -> None:
        """Set the current device state.
        """
        cmd = self._pe42420.config_state(self._state)
        write_cmd(ser, cmd)
