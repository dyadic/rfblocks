from time import sleep
import subprocess
import pytest
import pytestqt
from rfblocks import (
    ad9552, AD9552Controller, create_serial
)


@pytest.fixture(scope="module")
def ctl_device():
    serial_cmd_log = 'ad9552-serial-cmds.log'
    p = subprocess.Popen(
        ['socat', 'PTY,link=./virtual-tty,raw,echo=0',
         f"EXEC:'../../scripts/virt_ecapp.py -O {serial_cmd_log}'"])
    # Wait for a couple of seconds to ensure that the serial
    # device is in place.
    sleep(2)
    yield './virtual-tty'
    sleep(2)
    p.terminate()
    p = None


@pytest.fixture
def controller():
    clk = ad9552('d0', 'c4', 'c5', fref=26.0)
    clk_ctl = AD9552Controller('Clk 1', clk)
    return clk_ctl


@pytest.fixture
def controller_2():
    clk = ad9552('d1', 'c5', 'c6', fref=10.0, refselect='b1')
    clk_ctl = AD9552Controller('Clk 2', clk)
    return clk_ctl


def test_set_freq(qtbot, controller):
    with qtbot.waitSignal(controller.freq_changed) as blocker:
        controller.freq = 1200.0
        assert blocker.args == [1200.0]

    # AD9552Controller should not emit freq_changed if the new
    # frequency value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.freq_changed,
                              timeout=2000) as blocker:
            controller.freq = 1200.0
            assert blocker.signal_triggered is False
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_refsrc(qtbot, controller):
    with qtbot.waitSignal(controller.refsrc_changed) as blocker:
        controller.refsrc = ad9552.ReferenceSource.EXTERNAL
        assert blocker.args == [ad9552.ReferenceSource.EXTERNAL]

    # AD9552Controller should not emit refsrc_changed if the new
    # reference source value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.refsrc_changed,
                              timeout=2000) as blocker:
            controller.refsrc = ad9552.ReferenceSource.EXTERNAL
            assert blocker.signal_triggered is False
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_lock_status(qtbot, controller):
    with qtbot.waitSignal(controller.lock_status_changed) as blocker:
        controller.lock_status = True
        assert blocker.args == [True]

    # AD9552Controller should not emit lock_status_changed if the new
    # lock status value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.lock_status_changed,
                              timeout=2000) as blocker:
            controller.lock_status = True
            assert blocker.signal_triggered is False
    except pytestqt.exceptions.TimeoutError:
        pass


def test_ctl_initialize(controller, ctl_device):
    with create_serial(ctl_device, 38400) as ser:
        controller.initialize(ser)
        assert controller.freq == AD9552Controller.DEFAULT_CLKOUT_FREQ
        assert controller.lock_status is True


def test_ctl_configure(controller, ctl_device):
    controller.freq = 644.5
    with create_serial(ctl_device, 38400) as ser:
        controller.configure(ser)
        assert controller.freq == 644.5
        assert controller.lock_status is True


def test_set_chan_state(qtbot, controller):
    chan1 = controller.channels['1']
    with qtbot.waitSignal(chan1.state_changed) as blocker:
        chan1.state = ad9552.OutputState.ACTIVE
        assert blocker.args == [1, ad9552.OutputState.ACTIVE]

    # AD9552Channel should not emit state_changed if the new
    # state value is the same as the currently set value.
    try:
        with qtbot.waitSignal(chan1.state_changed,
                              timeout=2000) as blocker:
            chan1.state = ad9552.OutputState.ACTIVE
            assert blocker.signal_triggered is False
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_chan_mode(qtbot, controller):
    chan1 = controller.channels['1']
    with qtbot.waitSignal(chan1.mode_changed) as blocker:
        chan1.mode = ad9552.OutputMode.CMOS_BOTH_ACTIVE
        assert blocker.args == [1, ad9552.OutputMode.CMOS_BOTH_ACTIVE]

    # AD9552Channel should not emit mode_changed if the new
    # mode value is the same as the currently set value.
    try:
        with qtbot.waitSignal(chan1.mode_changed, timeout=2000) as blocker:
            chan1.mode = ad9552.OutputMode.CMOS_BOTH_ACTIVE
            assert blocker.signal_triggered is False
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_chan_drive(qtbot, controller):
    chan2 = controller.channels['2']
    with qtbot.waitSignal(chan2.drive_changed) as blocker:
        chan2.drive = ad9552.DriveStrength.WEAK
        assert blocker.args == [2, ad9552.DriveStrength.WEAK]

    # AD9552Channel should not emit drive_changed if the new
    # drive strength value is the same as the currently set value.
    try:
        with qtbot.waitSignal(chan2.drive_changed,
                              timeout=2000) as blocker:
            chan2.mode = ad9552.DriveStrength.WEAK
            assert blocker.signal_triggered is False
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_chan_polarity(qtbot, controller):
    chan2 = controller.channels['2']
    with qtbot.waitSignal(chan2.polarity_changed) as blocker:
        chan2.polarity = ad9552.CmosPolarity.DIFF_NEG
        assert blocker.args == [2, ad9552.CmosPolarity.DIFF_NEG]

    # AD9552Channel should not emit polarity_changed if the new
    # polarity value is the same as the currently set value.
    try:
        with qtbot.waitSignal(chan2.polarity_changed,
                              timeout=2000) as blocker:
            chan2.mode = ad9552.CmosPolarity.DIFF_NEG
            assert blocker.signal_triggered is False
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_chan_source(qtbot, controller):
    chan1 = controller.channels['1']
    with qtbot.waitSignal(chan1.source_changed) as blocker:
        chan1.source = ad9552.SourceControl.REF
        assert blocker.args == [1, ad9552.SourceControl.REF]

    # AD9552Channel should not emit source_changed if the new
    # source value is the same as the currently set value.
    try:
        with qtbot.waitSignal(chan1.source_changed,
                              timeout=2000) as blocker:
            chan1.source = ad9552.SourceControl.REF
            assert blocker.signal_triggered is False
    except pytestqt.exceptions.TimeoutError:
        pass


def test_dump_and_load(controller, controller_2):
    controller_2.freq = 644.52
    controller_2.channels['1'].state = ad9552.OutputState.POWERED_DOWN
    controller_2.channels['1'].mode = ad9552.OutputMode.CMOS_BOTH_ACTIVE
    controller_2.channels['1'].drive = ad9552.DriveStrength.WEAK
    controller_2.channels['2'].mode = ad9552.OutputMode.LVDS
    config_2 = controller_2.dump_config()
    controller.load_config(config_2)

    assert controller.freq == 644.52
    assert controller.channels['1'].state == ad9552.OutputState.POWERED_DOWN
    assert controller.channels['1'].mode == ad9552.OutputMode.CMOS_BOTH_ACTIVE
    assert controller.channels['1'].drive == ad9552.DriveStrength.WEAK
    assert controller.channels['2'].mode == ad9552.OutputMode.LVDS
