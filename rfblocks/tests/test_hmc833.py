"""
"""
import unittest
from rfblocks import hmc833


class hmc833Test(unittest.TestCase):

    def setUp(self):
        self.maxDiff = None
        self.plo1 = hmc833('d1', 'c5', fref=50.0)
        self.plo2 = hmc833('d2', 'c6', fref=250.0, refdiv=4,
                           refsrc=hmc833.ReferenceSource.EXTERNAL)

    def test_create(self):
        self.assertEqual(self.plo1.__str__(),
                'HMC833: fref:  50.00, sen: D1, ld_sdo: C5, refdiv: 1')
        self.assertEqual(self.plo2.__str__(),
                'HMC833: fref: 250.00, sen: D2, ld_sdo: C6, refdiv: 4')

    def test_pin_config(self):
        self.assertEqual(self.plo1.pin_config(),
                         'OD1:LD1:IC5:W00:')
        self.assertEqual(self.plo1.chip_reset(),
                         'W00,00,20,00:HD1:LD1:')

    def test_divider_values(self):
        n_int, n_frac, doubler, divide_ratio = self.plo1.divider_values(47.25)
        self.assertEqual((n_int, n_frac, doubler, divide_ratio),
                         (58, 9898557, False, 62))
        n_int, n_frac, doubler, divide_ratio = self.plo1.divider_values(107.5)
        self.assertEqual((n_int, n_frac, doubler, divide_ratio),
                         (55, 15099494, False, 26))
        n_int, n_frac, doubler, divide_ratio = self.plo1.divider_values(640.0)
        self.assertEqual((n_int, n_frac, doubler, divide_ratio),
                         (51, 3355443, False, 4))
        n_int, n_frac, doubler, divide_ratio = self.plo1.divider_values(1056.5)
        self.assertEqual((n_int, n_frac, doubler, divide_ratio),
                         (42, 4362076, False, 2))
        n_int, n_frac, doubler, divide_ratio = self.plo1.divider_values(2113.5)
        self.assertEqual((n_int, n_frac, doubler, divide_ratio),
                         (42, 4529848, False, 1))
        n_int, n_frac, doubler, divide_ratio = self.plo1.divider_values(3912.1)
        self.assertEqual((n_int, n_frac, doubler, divide_ratio),
                         (39, 2030043, True, 1))
        n_int, n_frac, doubler, divide_ratio = self.plo1.divider_values(5908.0)
        self.assertEqual((n_int, n_frac, doubler, divide_ratio),
                         (59, 1342177, True, 1))

    def test_frequency_cmds(self):
        cmd = self.plo1.device_initialize(1000.0)
        self.assertEqual(cmd,
                         """W00,00,28,18:HD1:LD1:W00,01,88,28:HD1:LD1:W00,01,10,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        cmd = self.plo1.config_frequency(783.0)
        self.assertEqual(cmd,
                         """W00,16,28,28:HD1:LD1:W00,60,A0,28:HD1:LD1:W00,01,88,28:HD1:LD1:W00,01,10,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:W03,0F,4A,30:HD1:LD1:W00,21,4d,38:HD1:LD1:WC1,BE,FF,40:HD1:LD1:W5C,BF,FF,48:HD1:LD1:W00,20,46,50:HD1:LD1:W07,C0,61,58:HD1:LD1:W00,00,C1,78:HD1:LD1:W00,00,1f,18:HD1:LD1:W51,eb,85,20:HD1:LD1:W00,00,1f,18:HD1:LD1:W51,eb,85,20:HD1:LD1:W00,01,88,28:HD1:LD1:W00,01,10,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        cmd = self.plo1.config_frequency(1000.0)
        self.assertEqual(cmd,
                         """W00,16,28,28:HD1:LD1:W00,60,A0,28:HD1:LD1:W00,01,88,28:HD1:LD1:W00,01,10,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:W03,07,CA,30:HD1:LD1:W00,21,4d,38:HD1:LD1:WC1,BE,FF,40:HD1:LD1:W1C,BF,FF,48:HD1:LD1:W00,20,46,50:HD1:LD1:W07,C0,61,58:HD1:LD1:W00,00,C1,78:HD1:LD1:W00,00,28,18:HD1:LD1:W00,00,28,18:HD1:LD1:W00,01,88,28:HD1:LD1:W00,01,10,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        cmd = self.plo1.config_frequency(1056.0)
        self.assertEqual(cmd,
                         """W00,16,28,28:HD1:LD1:W00,60,A0,28:HD1:LD1:W00,01,88,28:HD1:LD1:W00,01,10,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:W03,0F,4A,30:HD1:LD1:W00,21,4d,38:HD1:LD1:WC1,BE,FF,40:HD1:LD1:W5C,BF,FF,48:HD1:LD1:W00,20,46,50:HD1:LD1:W07,C0,61,58:HD1:LD1:W00,00,C1,78:HD1:LD1:W00,00,2a,18:HD1:LD1:W3d,70,a4,20:HD1:LD1:W00,00,2a,18:HD1:LD1:W3d,70,a4,20:HD1:LD1:W00,01,88,28:HD1:LD1:W00,01,10,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        cmd = self.plo1.config_frequency(2000)
        self.assertEqual(cmd,
                         """W00,16,28,28:HD1:LD1:W00,60,A0,28:HD1:LD1:W00,01,88,28:HD1:LD1:W00,00,90,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:W03,07,CA,30:HD1:LD1:W00,21,4d,38:HD1:LD1:WC1,BE,FF,40:HD1:LD1:W1C,BF,FF,48:HD1:LD1:W00,20,46,50:HD1:LD1:W07,C0,61,58:HD1:LD1:W00,00,C1,78:HD1:LD1:W00,00,28,18:HD1:LD1:W00,00,28,18:HD1:LD1:W00,01,88,28:HD1:LD1:W00,00,90,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        cmd = self.plo1.config_frequency(50.0)
        self.assertEqual(cmd,
                         """W00,00,3c,18:HD1:LD1:W00,01,88,28:HD1:LD1:W00,1E,10,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        cmd = self.plo1.config_frequency(2113.5)
        self.assertEqual(cmd,
                         """W00,16,28,28:HD1:LD1:W00,60,A0,28:HD1:LD1:W00,01,88,28:HD1:LD1:W00,00,90,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:W03,0F,4A,30:HD1:LD1:W00,21,4d,38:HD1:LD1:WC1,BE,FF,40:HD1:LD1:W5C,BF,FF,48:HD1:LD1:W00,20,46,50:HD1:LD1:W07,C0,61,58:HD1:LD1:W00,00,C1,78:HD1:LD1:W00,00,2a,18:HD1:LD1:W45,1e,b8,20:HD1:LD1:W00,00,2a,18:HD1:LD1:W45,1e,b8,20:HD1:LD1:W00,01,88,28:HD1:LD1:W00,00,90,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        cmd = self.plo1.config_frequency(3017.5)
        self.assertEqual(cmd,
                         """W00,00,1e,18:HD1:LD1:W2c,cc,cd,20:HD1:LD1:W00,01,88,28:HD1:LD1:W00,00,90,28:HD1:LD1:W00,20,18,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")

    def test_gain_cmds(self):
        cmd = self.plo1.device_initialize(1000.0)
        self.assertEqual(cmd,
                         """W00,00,20,00:HD1:LD1:W00,00,02,08:HD1:LD1:W00,00,01,10:HD1:LD1:W00,16,28,28:HD1:LD1:W00,60,A0,28:HD1:LD1:W00,01,88,28:HD1:LD1:W00,01,10,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:W03,07,CA,30:HD1:LD1:W00,21,4d,38:HD1:LD1:WC1,BE,FF,40:HD1:LD1:W1C,BF,FF,48:HD1:LD1:W00,20,46,50:HD1:LD1:W07,C0,61,58:HD1:LD1:W00,00,C1,78:HD1:LD1:W00,00,28,18:HD1:LD1:""")
        cmd = self.plo1.config_frequency(50.0)
        self.assertEqual(cmd,
                         """W00,00,3c,18:HD1:LD1:W00,01,88,28:HD1:LD1:W00,1E,10,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        self.plo1.auto_gain(50.0)
        cmd = self.plo1.config_gain()
        self.assertEqual(cmd,
                         """W00,01,88,28:HD1:LD1:W00,10,10,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        self.plo1.buf_gain = hmc833.OutputBufferGain.MAXGAIN_MINUS_6DB
        cmd = self.plo1.config_gain()
        self.assertEqual(cmd,
                         """W00,01,88,28:HD1:LD1:W00,30,10,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")

        self.plo1.buf_gain = hmc833.OutputBufferGain.MAXGAIN_MINUS_3DB
        cmd = self.plo1.config_gain()
        self.assertEqual(cmd,
                         """W00,01,88,28:HD1:LD1:W00,50,10,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")

        self.plo1.buf_gain = hmc833.OutputBufferGain.MAXGAIN
        cmd = self.plo1.config_gain()
        self.assertEqual(cmd,
                         """W00,01,88,28:HD1:LD1:W00,70,10,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")

        self.plo1.div_gain = hmc833.DividerGain.MAXGAIN
        cmd = self.plo1.config_gain()
        self.assertEqual(cmd,
                         """W00,01,88,28:HD1:LD1:W00,F0,10,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")

        cmd = self.plo1.config_frequency(1750.0)
        self.plo1.auto_gain(1750.0)
        cmd = self.plo1.config_gain()
        self.assertEqual(cmd,
                         """W00,01,88,28:HD1:LD1:W00,80,90,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        self.plo1.buf_gain = hmc833.OutputBufferGain.MAXGAIN_MINUS_6DB
        cmd = self.plo1.config_gain()
        self.assertEqual(cmd,
                         """W00,01,88,28:HD1:LD1:W00,A0,90,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        self.plo1.buf_gain = hmc833.OutputBufferGain.MAXGAIN_MINUS_3DB
        cmd = self.plo1.config_gain()
        self.assertEqual(cmd,
                         """W00,01,88,28:HD1:LD1:W00,C0,90,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        self.plo1.buf_gain = hmc833.OutputBufferGain.MAXGAIN
        cmd = self.plo1.config_gain()
        self.assertEqual(cmd,
                         """W00,01,88,28:HD1:LD1:W00,E0,90,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        self.plo1.div_gain = hmc833.DividerGain.MAXGAIN
        cmd = self.plo1.config_gain()
        self.assertEqual(cmd,
                         """W00,01,88,28:HD1:LD1:W00,E0,90,28:HD1:LD1:W00,28,98,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")

        cmd = self.plo1.config_frequency(4017.5)
        self.plo1.auto_gain(4017.5)
        cmd = self.plo1.config_gain()
        self.assertEqual(cmd,
                         """W00,01,88,28:HD1:LD1:W00,E0,90,28:HD1:LD1:W00,20,18,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        self.plo1.buf_gain = hmc833.OutputBufferGain.MAXGAIN_MINUS_6DB
        cmd = self.plo1.config_gain()
        self.assertEqual(cmd,
                         """W00,01,88,28:HD1:LD1:W00,A0,90,28:HD1:LD1:W00,20,18,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        self.plo1.buf_gain = hmc833.OutputBufferGain.MAXGAIN_MINUS_3DB
        cmd = self.plo1.config_gain()
        self.assertEqual(cmd,
                         """W00,01,88,28:HD1:LD1:W00,C0,90,28:HD1:LD1:W00,20,18,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        self.plo1.buf_gain = hmc833.OutputBufferGain.MAXGAIN
        cmd = self.plo1.config_gain()
        self.assertEqual(cmd,
                         """W00,01,88,28:HD1:LD1:W00,E0,90,28:HD1:LD1:W00,20,18,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
        self.plo1.div_gain = hmc833.DividerGain.MAXGAIN
        cmd = self.plo1.config_gain()
        self.assertEqual(cmd,
                         """W00,01,88,28:HD1:LD1:W00,E0,90,28:HD1:LD1:W00,20,18,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")

        self.plo1.mute_vco = True
        cmd = self.plo1.config_vco_mute()
        self.assertEqual(cmd,
                         """W00,01,88,28:HD1:LD1:W00,E0,90,28:HD1:LD1:W00,22,18,28:HD1:LD1:W00,00,00,28:HD1:LD1:""")
