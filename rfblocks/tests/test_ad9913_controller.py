from time import sleep
import subprocess
import pytest
import pytestqt
from rfblocks import (
    ad9913, AD9913Controller, create_serial
)


@pytest.fixture(scope="module")
def ctl_device():
    serial_cmd_log = 'ad9913-serial-cmds.log'
    p = subprocess.Popen(
        ['socat', 'PTY,link=./virtual-tty,raw,echo=0',
         f"EXEC:'../../scripts/virt_ecapp.py -O {serial_cmd_log}'"])
    # Wait for a couple of seconds to ensure that the serial
    # device is in place.
    sleep(2)
    yield './virtual-tty'
    sleep(2)
    p.terminate()
    p = None


@pytest.fixture
def controller():
    hwconf = {'cs': 'C6', 'io_update': 'B6', 'reset': 'C4',
                    'ps0': 'D4', 'ps1': 'D5', 'ps2': 'D6',
                    'board_model': '27dB-RF'}
    dds = ad9913(**hwconf)
    dds_ctl = AD9913Controller('Chan 1', dds)
    return dds_ctl


@pytest.fixture
def controller_2():
    hwconf = {'cs': 'C5', 'io_update': 'B7', 'reset': 'C4',
              'ps0': 'D0', 'ps1': 'D1', 'ps2': 'D2',
              'board_model': '20dB'}
    dds = ad9913(**hwconf)
    dds_ctl = AD9913Controller('Chan 2', dds)
    return dds_ctl


def test_set_state(qtbot, controller):
    with qtbot.waitSignal(controller.state_changed) as blocker:
        controller.state = ad9913.POWER_DOWN
        assert blocker.args == [ad9913.POWER_DOWN]

    # AD9913Controller should not emit state_changed if the new
    # state value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.state_changed,
                              timeout=2000) as blocker:
            controller.state = ad9913.POWER_DOWN
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_freq(qtbot, controller):
    with qtbot.waitSignal(controller.freq_changed) as blocker:
        controller.freq = 12.56
        assert blocker.args == [12.56]

    # AD9913Controller should not emit freq_changed if the new
    # frequency value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.freq_changed,
                              timeout=2000) as blocker:
            controller.freq = 12.56
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_phase(qtbot, controller):
    with qtbot.waitSignal(controller.phase_changed) as blocker:
        controller.phase = 120.0
        assert blocker.args == [120.0]

    # AD9913Controller should not emit phase_changed if the new
    # phase value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.phase_changed,
                              timeout=2000) as blocker:
            controller.freq = 120.0
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_level(qtbot, controller):
    with qtbot.waitSignal(controller.level_changed) as blocker:
        controller.level = 256
        assert blocker.args == [256]

    # AD9913Controller should not emit level_changed if the new
    # level value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.level_changed,
                              timeout=2000) as blocker:
            controller.level = 256
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_millivolts(qtbot, controller_2):
    with qtbot.waitSignal(controller_2.level_changed) as blocker:
        controller_2.millivolts = 100
        assert blocker.args == [168]


def test_set_dbm(qtbot, controller_2):
    with qtbot.waitSignal(controller_2.level_changed) as blocker:
        controller_2.dbm = -10.0
        assert blocker.args == [118]


def test_set_pmod(qtbot, controller):
    with qtbot.waitSignal(controller.pmod_changed) as blocker:
        controller.pmod = True
        assert blocker.args == [True]

    # AD9913Controller should not emit pmod_changed if the new
    # pmod value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.pmod_changed,
                              timeout=2000) as blocker:
            controller.pmod = True
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_selected_profile(qtbot, controller):
    with qtbot.waitSignal(controller.selected_profile_changed) as blocker:
        controller.selected_profile = 4
        assert blocker.args == [4]

    # AD9913Controller should not emit selected_profile_changed if the new
    # selected_profile value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.selected_profile_changed,
                              timeout=2000) as blocker:
            controller.selected_profile = 4
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_profile_type(qtbot, controller):
    with qtbot.waitSignal(controller.profile_type_changed) as blocker:
        controller.profile_type = ad9913.SweepType.PHASE
        assert blocker.args == [ad9913.SweepType.PHASE]

    # AD9913Controller should not emit profile_type_changed if the new
    # profile_type value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.profile_type_changed,
                              timeout=2000) as blocker:
            controller.profile_type = ad9913.SweepType.PHASE
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_sweep_start(qtbot, controller):
    with qtbot.waitSignal(controller.sweep_start_changed) as blocker:
        controller.sweep_start = 15.0
        assert blocker.args == [15.0]

    # AD9913Controller should not emit sweep_start_changed if the new
    # sweep_start value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.sweep_start_changed,
                              timeout=2000) as blocker:
            controller.sweep_start = 15.0
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_sweep_end(qtbot, controller):
    with qtbot.waitSignal(controller.sweep_end_changed) as blocker:
        controller.sweep_end = 60.0
        assert blocker.args == [60.0]

    # AD9913Controller should not emit sweep_end_changed if the new
    # sweep_end value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.sweep_end_changed,
                              timeout=2000) as blocker:
            controller.sweep_end = 60.0
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_sweep_type(qtbot, controller):
    with qtbot.waitSignal(controller.sweep_type_changed) as blocker:
        controller.sweep_type = ad9913.SweepType.PHASE
        assert blocker.args == [ad9913.SweepType.PHASE]

    # AD9913Controller should not emit sweep_type_changed if the new
    # sweep_type value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.sweep_type_changed,
                              timeout=2000) as blocker:
            controller.sweep_type = ad9913.SweepType.PHASE
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_ramp_type(qtbot, controller):
    with qtbot.waitSignal(controller.ramp_type_changed) as blocker:
        controller.sweep_ramp_type = ad9913.SweepRampType.RAMP_BIDIR
        assert blocker.args == [ad9913.SweepRampType.RAMP_BIDIR]

    # AD9913Controller should not emit sweep_ramp_type_changed if the new
    # sweep_ramp_type value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.ramp_type_changed,
                              timeout=2000) as blocker:
            controller.sweep_ramp_type = ad9913.SweepRampType.RAMP_BIDIR
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_rising_step(qtbot, controller):
    with qtbot.waitSignal(controller.rising_step_changed) as blocker:
        controller.sweep_rising_step = 0.001
        assert blocker.args == [0.001]

    # AD9913Controller should not emit rising_step_changed if the new
    # sweep_rising_step value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.rising_step_changed,
                              timeout=2000) as blocker:
            controller.sweep_rising_step = 0.001
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_falling_step(qtbot, controller):
    with qtbot.waitSignal(controller.falling_step_changed) as blocker:
        controller.sweep_falling_step = 0.001
        assert blocker.args == [0.001]

    # AD9913Controller should not emit falling_step_changed if the new
    # sweep_falling_step value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.falling_step_changed,
                              timeout=2000) as blocker:
            controller.sweep_falling_step = 0.001
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_rising_rate(qtbot, controller):
    with qtbot.waitSignal(controller.rising_rate_changed) as blocker:
        controller.sweep_rising_rate = 1.0
        assert blocker.args == [1.0]

    # AD9913Controller should not emit rising_rate_changed if the new
    # sweep_rising_rate value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.rising_rate_changed,
                              timeout=2000) as blocker:
            controller.sweep_rising_rate = 1.0
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_falling_rate(qtbot, controller):
    with qtbot.waitSignal(controller.falling_rate_changed) as blocker:
        controller.sweep_falling_rate = 1.0
        assert blocker.args == [1.0]

    # AD9913Controller should not emit falling_rate_changed if the new
    # sweep_falling_rate value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.falling_rate_changed,
                              timeout=2000) as blocker:
            controller.sweep_falling_rate = 1.0
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_profile_freq(qtbot, controller):
    with qtbot.waitSignal(controller.profile_freq_changed) as blocker:
        controller.set_profile_freq(4, 31.0)
        assert blocker.args == [4, 31.0]

    # AD9913Controller should not emit profile_freq_changed if the new
    # profile freq. value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.profile_freq_changed,
                              timeout=2000) as blocker:
            controller.set_profile_freq(4, 31.0)
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_set_profile_phase(qtbot, controller):
    with qtbot.waitSignal(controller.profile_phase_changed) as blocker:
        controller.set_profile_phase(3, 27.5)
        assert blocker.args == [3, 27.5]

    # AD9913Controller should not emit profile_phase_changed if the new
    # profile phase value is the same as the currently set value.
    try:
        with qtbot.waitSignal(controller.profile_phase_changed,
                              timeout=2000) as blocker:
            controller.set_profile_phase(3, 27.5)
            assert blocker.signal_triggered is not True
    except pytestqt.exceptions.TimeoutError:
        pass


def test_ctl_initialize(controller, ctl_device):
    with create_serial(ctl_device, 38400) as ser:
        controller.initialize(ser)
        assert controller.freq == AD9913Controller.DEFAULT_DDS_FREQ


def test_ctl_configure(controller, ctl_device):
    controller.freq = 34.17
    with create_serial(ctl_device, 38400) as ser:
        controller.configure(ser)
        assert controller.freq == 34.17
