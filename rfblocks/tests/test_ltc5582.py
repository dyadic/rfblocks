"""
"""
import unittest
import io
from rfblocks import ltc5582


class ltc5582Test(unittest.TestCase):

    def setUp(self):
        self.det = ltc5582('d0')

    def test_create(self):
        self.assertEqual(self.det.__str__(), 'LTC5582: cs: D0')

    def test_pin_config(self):
        self.assertEqual(self.det.pin_config(),
                         'OD0:HD0:')

    def test_read_raw(self):
        output = io.StringIO()
        self.det.read_raw(output)
        self.assertEqual(output.getvalue(),
                         'LD0:S3:\nW40:\nW01:\nR:\nW02:\nR:\nHD0:S1:\n')
        output.close()

    def test_initialize(self):
        output = io.StringIO()
        self.det.initialize(output)
        self.assertEqual(output.getvalue(),
                    'LD0:S3:\nW64:\nR:\nW65:\nR:\nW66:\nR:\nW67:\nR:\nW68:\nR:\nW69:\nR:\nW6A:\nR:\nW6B:\nR:\nW6C:\nR:\nW6D:\nR:\nW6E:\nR:\nW6F:\nR:\nW70:\nR:\nW71:\nR:\nW72:\nR:\nW73:\nR:\nW74:\nR:\nW75:\nR:\nW76:\nR:\nW77:\nR:\nW78:\nR:\nW79:\nR:\nW7A:\nR:\nW7B:\nR:\nW7C:\nR:\nW7D:\nR:\nW7E:\nR:\nW7F:\nR:\nW80:\nR:\nW81:\nR:\nW82:\nR:\nW83:\nR:\nW84:\nR:\nW85:\nR:\nW86:\nR:\nW87:\nR:\nHD0:S1:\n')
        output.close()
        
    def test_power(self):
        output = io.StringIO()
        pwr = self.det.power(output, 4000)
        self.assertEqual(output.getvalue(),
                    'LD0:S3:\nW64:\nR:\nW65:\nR:\nW66:\nR:\nW67:\nR:\nW68:\nR:\nW69:\nR:\nW6A:\nR:\nW6B:\nR:\nW6C:\nR:\nW6D:\nR:\nW6E:\nR:\nW6F:\nR:\nW70:\nR:\nW71:\nR:\nW72:\nR:\nW73:\nR:\nW74:\nR:\nW75:\nR:\nW76:\nR:\nW77:\nR:\nW78:\nR:\nW79:\nR:\nW7A:\nR:\nW7B:\nR:\nW7C:\nR:\nW7D:\nR:\nW7E:\nR:\nW7F:\nR:\nW80:\nR:\nW81:\nR:\nW82:\nR:\nW83:\nR:\nW84:\nR:\nW85:\nR:\nW86:\nR:\nW87:\nR:\nHD0:S1:\nLD0:S3:\nW40:\nW01:\nR:\nW02:\nR:\nHD0:S1:\n')
        self.assertTrue(pwr >= 0.0)


