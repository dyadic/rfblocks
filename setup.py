from setuptools import setup, find_packages

setup(
    name='rfblocks',
    version='0.1.0',    
    description='RF hardware module control',
    url='https://www.dyadicindustries.com',
    author='Dyadic Pty Ltd',
    author_email='dyadic.industries@gmail.com',
    license='GNU LGPLv3',
    packages=find_packages(),
    install_requires=[
        'pyserial>=3.4',
        'rpyc>=5.0.1',
        'qasync>=0.16.0',
        'PyQt5',
        'pyqtgraph',
        'scipy'
    ],
    python_requires=">=3.7",
    test_suite='nose.collector',
    tests_require=['nose'],
    package_data={'rfblocks': ['data/*.s1p'], 'tam': ['data/*.json']},
    scripts=['scripts/virt_ecapp.py'],

    classifiers=[
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
    ],
)
