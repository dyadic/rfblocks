#

class SMHU58(object):

    MIN_FREQUENCY = 0.1
    MAX_FREQUENCY = 4320.0

    MIN_PWR = -140.0
    MAX_PWR = 13.0

    def __init__(self, gpib_controller, gpib_id):
        self.gpib = gpib_controller
        self.gpib_id = gpib_id

    def local(self):
        self.gpib.gpib_id = self.gpib_id
        self.gpib.local()

    def read_float(self):
        sval = self.gpib.read()
        return float(sval.strip().split()[-1])

    def cmd(self, cmd):
        self.gpib.gpib_id = self.gpib_id
        self.gpib.write(cmd)

    @property
    def ident(self):
        """Returns the instrument ID string.

        :raises: socket.timeout   If the instrument is unavailable
        """
        _ident = None
        if self.gpib:
            self.gpib.gpib_id = self.gpib_id
            self.gpib.clear()
            self.gpib.write("*IDN?")
            _ident = self.gpib.gpib.read()
        return _ident

    @property
    def output(self):
        """"""
        self.gpib.gpib_id = self.gpib_id
        self.gpib.write("LEV?")
        resp = self.gpib.read().strip().split()
        val = resp[0].split(':')[-1]
        if val == 'OFF':
            return False
        else:
            return True

    @property
    def freq(self):
        """Return the current SMHU58 output frequency (in MHz)."""
        self.gpib.gpib_id = self.gpib_id
        self.gpib.write("RF?")
        freq = self.read_float() / 1e6
        return freq

    @property
    def level(self):
        """Return the current SMHU58 output level (in dBm)."""
        self.gpib.gpib_id = self.gpib_id
        self.gpib.write("LEV?")
        level = self.read_float()
        return level

    @output.setter
    def output(self, state):
        """"""
        self.gpib.gpib_id = self.gpib_id
        if state is True:
            self.gpib.write("LEV:ON")
        else:
            self.gpib.write("LEV:OFF")

    @freq.setter
    def freq(self, f):
        """Set the output frequency to 'f' MHz."""
        self.gpib.gpib_id = self.gpib_id
        self.gpib.write("RF {}MHZ".format(f))

    @level.setter
    def level(self, lvl):
        """Set the output level to 'lvl' dBm."""
        self.gpib.gpib_id = self.gpib_id
        self.gpib.write("LEV {}DBM".format(lvl))

    @property
    def level_units(self) -> str:
        return 'DBM'

    @level_units.setter
    def level_units(self, unit: str) -> None:
        pass

    def set_signal_output(self, freq, level):
        """Set the RF output.

        Parameters
        ----------
        freq : float
             The desired output frequency in MHz.
        level : float
             The desired output level in dBm.
        """
        self.freq = freq
        self.level = level

    def initialize(self):
        pass

    def reset(self):
        self.local()
