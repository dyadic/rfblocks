from .visa import VisaDevice


class DG4162(VisaDevice):

    MIN_FREQUENCY = 0.1
    MAX_FREQUENCY = 160.0

    MIN_PWR = -45.0
    MAX_PWR = 18.0

    LEVEL_UNITS = ['VPP', 'VRMS', 'DBM']

    CHANNELS = 2

    def __init__(self, visa_rm, visa_id):
        super().__init__(visa_rm, visa_id)
        self._chan: int = 1

    def initialize(self):
        """Initialize the generator state.
        """
        super().initialize()
        self.chan = 0
        self.impedance = 50
        self.level_units = 'DBM'
        self.chan = 1
        self.impedance = 50
        self.level_units = 'DBM'
        self.chan = 0

    def local(self):
        if self.inst:
            self.inst.control_ren(0)

    def reset(self):
        if self.inst:
            pass

    @property
    def chan(self) -> int:
        """The output channel setting for subsequent instrument operations.
        """
        return self._chan

    @chan.setter
    def chan(self, ch: int) -> None:
        self._chan = ch + 1

    @property
    def chan_str(self) -> str:
        """Return a string representation of the current output channel
        setting.
        """
        return 'SOURCE{}'.format(self.chan)

    @property
    def output(self) -> bool:
        """The output channel state.
        True if the channel is enabled, False otherwise.
        """
        state = self.query(f"OUTPUT{self.chan}:STATE?")
        return True if state == 'ON' else False

    @output.setter
    def output(self, state: bool) -> None:
        state = 'ON' if state else 'OFF'
        self.write(f"OUTPUT{self.chan}:STATE {state}")

    @property
    def impedance(self) -> int:
        """The channel output impedance in Ohms.  The valid range for
        the output impedance is 1 to 10,000.
        """
        return self.query_int(f"OUTPUT{self.chan}:IMP?")

    @impedance.setter
    def impedance(self, r: int) -> None:
        self.write(f"OUTPUT{self.chan}:IMP {r}")

    @property
    def freq(self) -> float:
        """The channel output frequency (in MHz) as a :py:`float` value.
        """
        return self.query_float(f"SOURCE{self.chan}:FREQ?") / 1e6

    @freq.setter
    def freq(self, f: float) -> None:
        self.write(f"SOURCE{self.chan}:FREQ {f*1e6}")

    @property
    def level(self) -> float:
        """The channel output level as a :py:`float`.
        The units of the output level will depend on which level
        unit has been set for the output channel.
        See :py:`level_units`.
        """
        return self.query_float(f"SOURCE{self.chan}:VOLT?")

    @level.setter
    def level(self, lvl: float) -> None:
        self.write(f"SOURCE{self.chan}:VOLT {lvl}")

    @property
    def level_units(self) -> str:
        """The unit used for the channel output level.
        This is one of: :py:`['DAC', 'MV', 'DBM']`
        """
        return self.query(f"SOURCE{self.chan}:VOLT:UNIT?")

    @level_units.setter
    def level_units(self, unit: str) -> None:
        self.write(f"SOURCE{self.chan}:VOLT:UNIT {unit}")

    def set_signal_output(self, freq: float, level: float) -> None:
        """Set the channel output frequency and level.

        :param freq: The channel output frequency to set (in MHz)
        :type freq: float
        :param level: The channel output level to set (in the current
             channel level units).
        :type level: float
        """
        self.level = level
        self.freq = freq
