"""
"""
import unittest
import numpy as np
from rfblocks import hmc833, pe43711
from tam import TwoTone, OutOfRangeError, NoCalibrationDataError


class twoToneTest(unittest.TestCase):

    def setUp(self):
        self.twoTone = TwoTone()
        self.twoTone.initialize()

    def test_initialize(self):
        self.assertEqual(self.twoTone.chanA.freq, 1000.0)
        self.assertEqual(self.twoTone.chanB.freq, 1010.0)

    def test_freq(self):
        self.twoTone.chanA.freq = 2134.0
        self.assertEqual(self.twoTone.chanA.freq, 2134.0)
        self.twoTone.chanB.freq = 2136.0
        self.assertEqual(self.twoTone.chanB.freq, 2136.0)
        self.twoTone.channel('A').freq = 345.6
        self.assertEqual(self.twoTone.chanA.freq, 345.6)
        self.twoTone.channel('B').freq = 59.6
        self.assertEqual(self.twoTone.chanB.freq, 59.6)

    def test_atten(self):
        self.twoTone.chanA.atten = 20.0
        self.assertEqual(self.twoTone.chanA.level, -18.91)
        self.assertEqual(self.twoTone.chanA.atten, 20.0)
        self.twoTone.chanB.atten = 21.25
        self.assertEqual(self.twoTone.chanB.level, -19.875)
        self.assertEqual(self.twoTone.chanB.atten, 21.25)

    def test_set_rf_output(self):
        self.twoTone.chanA.set_rf_output(1125.0, -10.0)
        a = self.twoTone.chanA.atten
        self.assertEqual(int(a/0.25) * 0.25, 11.5)
        self.assertEqual(self.twoTone.chanA.freq, 1125.0)
        self.twoTone.chanB.set_rf_output(1127.0, -10.0)
        a = self.twoTone.chanB.atten
        self.assertEqual(int(a/0.25) * 0.25, 11.25)
        self.assertEqual(self.twoTone.chanB.freq, 1127.0)
        with self.assertRaises(OutOfRangeError):
            self.twoTone.chanA.set_rf_output(3200.0, -10.0)
        self.twoTone.chanA.caltable = None
        with self.assertRaises(NoCalibrationDataError):
            self.twoTone.chanA.set_rf_output(1125.0, -10.0)

    def test_calibrate(self):
        self.twoTone.chanA.calibrate(self.start_sweep,
                                     self.end_sweep,
                                     self.measure_pwr,
                                     np.arange(100, 200, 50.0))

    def start_sweep(self, freq):
        pass

    def end_sweep(self, freq):
        pass

    def measure_pwr(self, f, a):
        return 10.0 - a
