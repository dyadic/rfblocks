"""
"""
import unittest
from tam import GPIB, SMHU58_GPIBID, SMHU58


class smhu58Test(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.gpib_controller = GPIB()
        self.gpib_controller.initialize()

        self.smhu58 = SMHU58(self.gpib_controller,
                             SMHU58_GPIBID)
        self.smhu58.initialize()

    @classmethod
    def tearDownClass(self):
        self.smhu58.reset()
        self.smhu58.local()
        self.gpib_controller.close()

    def test_freq(self):
        self.smhu58.output = True
        for f in [100.0, 200.0, 300.0, 400.0, 500.0]:
            self.smhu58.freq = f
            self.assertEqual(self.smhu58.freq, f)

    def test_level(self):
        self.smhu58.output = True
        self.smhu58.freq = 1000.0
        for l in [-20.0, -25.0, -30.0, -35.0, -40.0, -45.0]:
            self.smhu58.level = l
            self.assertEqual(self.smhu58.level, l)

    def test_output(self):
        self.smhu58.output = True
        self.assertTrue(self.smhu58.output)
        self.smhu58.output = False
        self.assertFalse(self.smhu58.output)
        self.smhu58.output = True

    def test_rf_output(self):
        self.smhu58.set_rf_output(900.0, -10.0)
        self.assertEqual(self.smhu58.freq, 900.0)
        self.assertEqual(self.smhu58.level, -10.0)
        self.smhu58.set_rf_output(1200.0, -5.0)
        self.assertEqual(self.smhu58.freq, 1200.0)
        self.assertEqual(self.smhu58.level, -5.0)

