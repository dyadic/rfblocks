"""
"""
import unittest
from tam import GPIB, HP8560A_GPIBID, HP8560A


class hp8560aTest(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.gpib_controller = GPIB()
        self.gpib_controller.initialize()

        self.hp8560a = HP8560A(self.gpib_controller,
                               HP8560A_GPIBID)
        self.hp8560a.initialize()

    @classmethod
    def tearDownClass(self):
        self.hp8560a.err_check()
        self.hp8560a.reset()
        self.hp8560a.local()
        self.gpib_controller.close()

    def test_centre_freq(self):
        self.hp8560a.freq = 100.0
        self.assertEqual(self.hp8560a.freq, 100.0)
        self.hp8560a.freq = 500.0
        self.assertEqual(self.hp8560a.freq, 500.0)
        self.hp8560a.freq = 1012.0
        self.assertEqual(self.hp8560a.freq, 1012.0)

    def test_freq_span(self):
        self.hp8560a.freq = 1000.0
        self.assertEqual(self.hp8560a.freq, 1000.0)
        self.hp8560a.fspan = 0.1
        self.assertEqual(self.hp8560a.fspan, 0.1)

    def test_reflevel(self):
        self.hp8560a.freq = 1000.0
        self.assertEqual(self.hp8560a.freq, 1000.0)
        self.hp8560a.fspan = 0.1
        self.assertEqual(self.hp8560a.fspan, 0.1)
        self.hp8560a.ref_level = 0
        self.assertEqual(self.hp8560a.ref_level, 0)
        self.hp8560a.ref_level = -20
        self.assertEqual(self.hp8560a.ref_level, -20)
        self.hp8560a.ref_level = -30
        self.assertEqual(self.hp8560a.ref_level, -30)
        self.hp8560a.ref_level = 0
        self.assertEqual(self.hp8560a.ref_level, 0)

    def test_atten(self):
        self.hp8560a.freq = 1000.0
        self.assertEqual(self.hp8560a.freq, 1000.0)
        self.hp8560a.fspan = 2.0
        self.assertEqual(self.hp8560a.fspan, 2.0)
        self.hp8560a.atten = 20.0
        self.assertEqual(self.hp8560a.atten, 20.0)
        self.hp8560a.atten = 30.0
        self.assertEqual(self.hp8560a.atten, 30.0)
        self.hp8560a.atten = 40.0
        self.assertEqual(self.hp8560a.atten, 40.0)
        self.hp8560a.atten = 10.0
        self.assertEqual(self.hp8560a.atten, 10.0)

    def test_vavg(self):
        self.hp8560a.freq = 1000.0
        self.assertEqual(self.hp8560a.freq, 1000.0)
        self.hp8560a.fspan = 2.0
        self.assertEqual(self.hp8560a.fspan, 2.0)
        self.hp8560a.vavg = 10
        self.assertEqual(self.hp8560a.vavg, 10)
        self.hp8560a.vavg = 30
        self.assertEqual(self.hp8560a.vavg, 30)
        self.hp8560a.vavg = 1
        self.assertEqual(self.hp8560a.vavg, 1)
        
    def test_rbw(self):
        self.hp8560a.freq = 1000.0
        self.assertEqual(self.hp8560a.freq, 1000.0)
        self.hp8560a.fspan = 2.0
        self.assertEqual(self.hp8560a.fspan, 2.0)
        self.hp8560a.rbw = 1e5
        self.assertEqual(self.hp8560a.rbw, 1e5)
        self.hp8560a.rbw = 1e4
        self.assertEqual(self.hp8560a.rbw, 1e4)

    def test_rbw_auto(self):
        self.hp8560a.freq = 1000.0
        self.assertEqual(self.hp8560a.freq, 1000.0)
        self.hp8560a.fspan = 2.0
        self.assertEqual(self.hp8560a.fspan, 2.0)
        self.hp8560a.rbw = 1e5
        self.assertEqual(self.hp8560a.rbw, 1e5)
        self.hp8560a.rbw_auto = True
        self.hp8560a.rbw_auto = False
        self.hp8560a.rbw = 1e4
        self.assertEqual(self.hp8560a.rbw, 1e4)

    def test_scale(self):
        self.hp8560a.freq = 1000.0
        self.assertEqual(self.hp8560a.freq, 1000.0)
        self.hp8560a.fspan = 2.0
        self.assertEqual(self.hp8560a.fspan, 2.0)
        self.hp8560a.scale = 5
        self.assertEqual(self.hp8560a.scale, 5)
        self.hp8560a.scale = 2
        self.assertEqual(self.hp8560a.scale, 2)
        self.hp8560a.scale = 10
        self.assertEqual(self.hp8560a.scale, 10)
