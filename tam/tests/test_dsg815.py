"""
"""
import unittest
import pyvisa
from tam import DSG815

DSG815_ID = 'TCPIP0::192.168.0.199::INSTR'


class dsg815Test(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.visa_rm = pyvisa.ResourceManager('@py')
        self.dsg815 = DSG815(self.visa_rm, DSG815_ID)
        self.dsg815.initialize()

    @classmethod
    def tearDownClass(self):
        self.dsg815.reset()
        self.visa_rm.close()

    def test_freq(self):
        self.dsg815.output = True
        for f in [100.0, 200.0, 300.0, 400.0, 500.0]:
            self.dsg815.freq = f
            self.assertEqual(self.dsg815.freq, f)

    def test_level(self):
        self.dsg815.output = True
        self.dsg815.freq = 1000.0
        for l in [-20.0, -25.0, -30.0, -35.0, -40.0, -45.0]:
            self.dsg815.level = l
            self.assertEqual(self.dsg815.level, l)

    def test_output(self):
        self.dsg815.output = True
        self.assertTrue(self.dsg815.output)
        self.dsg815.output = False
        self.assertFalse(self.dsg815.output)
        self.dsg815.output = True

    def test_rf_output(self):
        self.dsg815.set_rf_output(900.0, -10.0)
        self.assertEqual(self.dsg815.freq, 900.0)
        self.assertEqual(self.dsg815.level, -10.0)
        self.dsg815.set_rf_output(1200.0, -5.0)
        self.assertEqual(self.dsg815.freq, 1200.0)
        self.assertEqual(self.dsg815.level, -5.0)
