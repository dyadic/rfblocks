"""
"""
import unittest
import pyvisa
from tam import DSA815

DSA815_ID = 'TCPIP0::192.168.0.70::INSTR'


class dsa815Test(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.visa_rm = pyvisa.ResourceManager('@py')
        self.dsa815 = DSA815(self.visa_rm, DSA815_ID)
        self.dsa815.initialize()

    @classmethod
    def tearDownClass(self):
        self.dsa815.reset()
        self.visa_rm.close()

    def test_centre_freq(self):
        self.dsa815.freq = 100.0
        self.assertEqual(self.dsa815.freq, 100.0)
        self.dsa815.freq = 500.0
        self.assertEqual(self.dsa815.freq, 500.0)
        self.dsa815.freq = 1012.0
        self.assertEqual(self.dsa815.freq, 1012.0)

    def test_freq_span(self):
        self.dsa815.freq = 1000.0
        self.assertEqual(self.dsa815.freq, 1000.0)
        self.dsa815.fspan = 0.1
        self.assertEqual(self.dsa815.fspan, 0.1)
    
    def test_reflevel(self):
        self.dsa815.freq = 1000.0
        self.assertEqual(self.dsa815.freq, 1000.0)
        self.dsa815.fspan = 0.1
        self.assertEqual(self.dsa815.fspan, 0.1)
        self.dsa815.ref_level = 0
        self.assertEqual(self.dsa815.ref_level, 0)
        self.dsa815.ref_level = -20
        self.assertEqual(self.dsa815.ref_level, -20)
        self.dsa815.ref_level = -30
        self.assertEqual(self.dsa815.ref_level, -30)
        self.dsa815.ref_level = 0
        self.assertEqual(self.dsa815.ref_level, 0)

    def test_atten(self):
        self.dsa815.freq = 1000.0
        self.assertEqual(self.dsa815.freq, 1000.0)
        self.dsa815.fspan = 2.0
        self.assertEqual(self.dsa815.fspan, 2.0)
        self.dsa815.atten = 20.0
        self.assertEqual(self.dsa815.atten, 20.0)
        self.dsa815.atten = 30.0
        self.assertEqual(self.dsa815.atten, 30.0)
        self.dsa815.atten = 10.0
        self.assertEqual(self.dsa815.atten, 10.0)

    def test_vavg(self):
        self.dsa815.freq = 1000.0
        self.assertEqual(self.dsa815.freq, 1000.0)
        self.dsa815.fspan = 2.0
        self.assertEqual(self.dsa815.fspan, 2.0)
        self.dsa815.vavg = 10
        self.assertEqual(self.dsa815.vavg, 10)
        self.dsa815.vavg = 30
        self.assertEqual(self.dsa815.vavg, 30)
        self.dsa815.vavg = 1
        self.assertEqual(self.dsa815.vavg, 1)

    def test_rbw(self):
        self.dsa815.freq = 1000.0
        self.assertEqual(self.dsa815.freq, 1000.0)
        self.dsa815.fspan = 2.0
        self.assertEqual(self.dsa815.fspan, 2.0)
        self.dsa815.rbw = 1e5
        self.assertEqual(self.dsa815.rbw, 1e5)
        self.dsa815.rbw = 1e4
        self.assertEqual(self.dsa815.rbw, 1e4)

    def test_rbw_auto(self):
        self.dsa815.freq = 1000.0
        self.assertEqual(self.dsa815.freq, 1000.0)
        self.dsa815.fspan = 2.0
        self.assertEqual(self.dsa815.fspan, 2.0)
        self.dsa815.rbw = 1e5
        self.assertEqual(self.dsa815.rbw, 1e5)
        self.dsa815.rbw_auto = True
        self.dsa815.rbw_auto = False
        self.dsa815.rbw = 1e4
        self.assertEqual(self.dsa815.rbw, 1e4)

