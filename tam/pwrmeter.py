import rpyc
rpyc.core.protocol.DEFAULT_CONFIG['allow_pickle'] = True


class PowerMeter(object):

    CHANNELS = 2

    def __init__(self, ipaddr, port):
        self._ipaddr = ipaddr
        self._port = port
        self._chan: int = 1

    def initialize(self):
        """Initialize the power meter state.
        """
        self._dev = rpyc.connect(self._ipaddr, self._port)
        self._dev.root.initialize()

    def reset(self):
        self._dev.close()

    @property
    def chan(self) -> int:
        """Get or set the power meter channel to use.
        """
        return self._chan

    @chan.setter
    def chan(self, ch: int) -> None:
        self._chan = ch
        
    def measure_pwr(self, freq, apply_correction=False):
        """Measure the channel signal power.

        :param freq: The signal frequency in MHz.
        :type freq: float
        :param apply_correction: If this is :py:`True` then insertion
            loss correction (if available) will be applied to the
            measured signal power.
        :type apply_correction: bool

        :return: The signal power in dBm.
        """
        self._dev.root.detectors[self.chan].freq = freq
        self._dev.root.detectors[self.chan].apply_correction = apply_correction
        self._dev.root.measure(self.chan)
        return self._dev.root.detectors[self.chan].pwr

    def calibrate(self, ctl_id, src_device, device_id,
                  src_pwr, startf, stopf, stepf):
        """Run a calibration on the selected power detector.

        :param ctl_id: The detector to calibrate (either 0 or 1)
        :type ctl_id: int
        :param src_device: The signal source being used to carry out
                           the calibration.  This will be one of:
                           'SMHU58', 'DSG815' or 'DDSGEN'.
        :type src_device: str
        :param device_id: The id of the calibrating signal source.
        :type device_id: str
        :param src_pwr: The signal power level used for the calibration
                        (in dBm).
        :type src_pwr: float
        :param startf: The calibration start frequency in MHz.
        :type startf: float
        :param stopf: The calibration stop frequency in MHz.
        :type stopf: float
        :param stepf: The frequency step for each calibration point in MHz.
        :type stepf: float

        .. note::

            Calibration is a long running process and is synchronous.
            For this reason the ~rpyc~ synchronous request timeout is
            disabled for the duration of the calibration.
        """
        old_timeout = self._dev._config['sync_request_timeout']
        self._dev._config['sync_request_timeout'] = None
        self._dev.root.calibrate(
            ctl_id, src_device, device_id, src_pwr, startf, stopf, stepf)
        self._dev._config['sync_request_timeout'] = old_timeout
