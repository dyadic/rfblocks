from .visa import VisaDevice


class DSG815(VisaDevice):

    MIN_FREQUENCY = 0.009
    MAX_FREQUENCY = 1500.0

    MIN_PWR = -110.0
    MAX_PWR = 13.0

    def __init__(self, visa_rm, visa_id):
        super().__init__(visa_rm, visa_id)

    @property
    def output(self):
        """"""
        return bool(int(self.query(":OUTP?").strip()))

    @property
    def freq(self):
        """The current DSG output frequency (in MHz)."""
        freq = float(self.query(":SOUR:FREQ?")) / 1e6
        return freq

    @property
    def level(self):
        """The current DSG output level (in dBm)."""
        level = float(self.query(":SOUR:LEV?"))
        return level

    @output.setter
    def output(self, state):
        """"""
        if state is True:
            self.write(":OUTP ON")
        else:
            self.write(":OUTP OFF")

    @freq.setter
    def freq(self, f):
        """Set the output frequency to 'f' MHz."""
        self.write(":SOUR:FREQ {}".format(f * 1e6))

    @level.setter
    def level(self, lvl):
        """Set the output level to 'lvl' dBm."""
        self.write(":SOUR:LEV {}".format(lvl))

    def set_signal_output(self, freq, level):
        """Set the RF output.

        :param freq: The desired output frequency in MHz.
        :type freq: float
        :param level: The desired output level in dBm.
        :type level: float
        """
        self.freq = freq
        self.level = level

    @property
    def fm_coupling(self):
        return self.query(":FM:EXT:COUP?")

    @fm_coupling.setter
    def fm_coupling(self, c):
        if c in ['AC', 'DC']:
            self.write(":FM:EXT:COUP {}".format(c))

    @property
    def fm_impedance(self):
        return self.query(":FM:EXT:IMP?")

    @fm_impedance.setter
    def fm_impedance(self, imp):
        if imp in ['50', '600', '100k']:
            self.write(":FM:EXT:IMP {}".format(imp))

    @property
    def fm_source(self):
        return self.query(":FM:SOUR?")

    @fm_source.setter
    def fm_source(self, src):
        if src in ['INT', 'EXT']:
            self.write(":FM:SOUR {}".format(src))

    @property
    def fm_deviation(self):
        """Return the current DSG FM max. deviation (in kHz)."""
        dev = float(self.query(":FM:DEV?")) / 1e3
        return dev

    @fm_deviation.setter
    def fm_deviation(self, dev):
        """Set the current DSG FM max. deviation (in kHz)."""
        self.write(":FM:DEV {}kHz".format(dev))

    def initialize(self):
        """Initialize the generator state.
        """
        super().initialize()

    def local(self):
        if self.inst:
            self.inst.control_ren(0)

    def reset(self):
        if self.inst:
            pass
