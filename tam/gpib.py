#
import re
import socket
from plx_gpib_ethernet import PrologixGPIBEthernet

PROLOGIX_IPADDR = '192.168.0.175'
TEKTDS_GPIBID = 1
HP8560A_GPIBID = 18
HP11729C_GPIBID = 6
SMHU58_GPIBID = 28


class GPIB(object):

    BAD_CHRS = re.compile('(Ew)|([:;><,/?]+)')

    def __init__(self, gpib_ip=PROLOGIX_IPADDR):
        self.ip = gpib_ip
        self._gpib_id = 0
        self.gpib = None

    def initialize(self, timeout=2):
        """Connect to the GPIB controller.
        """
        self.gpib = PrologixGPIBEthernet(PROLOGIX_IPADDR, timeout)
        self.gpib.connect()

    def close(self):
        """Close connection to the GPIB controller.
        """
        self.gpib.close()

    def local(self):
        """Enable front panel operation of the currently addressed
        instrument.
        """
        self.gpib._send('++loc')

    def clear(self):
        """Sends the Selected Device Clear (SDC) message to the currently
        specified GPIB address. 
        """
        self.gpib._send('++clr')

    def spoll(self):
        """Perform a serial poll of the instrument at the currently
        specified address.
        """
        self.gpib._send('++spoll')

    def srq(self):
        """Return the current state of the GPIB SRQ signal
        """
        self.gpib._send('++srq')

    def reset(self):
        """Perform a power-on reset of the controller.
        """
        self.gpib._send('++rst')

    def eot_char(self, ch):
        """Specifies the character to be appended to network output when
        :py:`eot_enable` is set to 1 and EOI is detected.
        """
        self.gpib._send('++eot_char {}'.format(ord(ch)))

    def eot_enable(self, f):
        """Enable or disable the appending of a user specified character
        (see :py:`eot_char`) to network output whenever EOI is detected
        while reading a character from the GPIB port.
        """
        if f:
            self.gpib._send('++eot_enable 1')
        else:
            self.gpib._send('++eot_enable 0')

    def read_after_write(self, b):
        if b:
            self.gpib._send('++auto 1')
        else:
            # disable read after write
            self.gpib._send('++auto 0')

    @property
    def gpib_id(self):
        """Return the ID of the currently selected instrument.
        """
        return self._gpib_id

    @gpib_id.setter
    def gpib_id(self, gid):
        """Select the specified instrument.

        :param gid: The GPIB ID for the instrument to select.
        """
        self._gpib_id = gid
        if self.gpib:
            self.gpib.select(self._gpib_id)

    def write(self, cmd):
        """Write a command to the selected controller.

        :param cmd: The command to send to the selected controller.
        :type cmd: str
            
        """
        self.gpib.write(cmd)

    def read(self):
        """Read a value from the selected device.

        :return: The read value as a string.
        """
        while True:
            try:
                val = self.gpib.read()
                return val
            except socket.timeout:
                pass

    def read_bin(self, num_bytes):
        """Read data from the currently selected instrument.

        :param num_bytes: The number of bytes of data to read.
        :type num_bytes: int

        :return: The data read from the instrument as a bytearray.
        """
        self.gpib._send('++read eoi')
        data = bytearray()
        byte_count = num_bytes
        while True:
            try:
                bytes_read = self.gpib.socket.recv(byte_count)
                data += bytes_read
                if len(data) == num_bytes:
                    return data
                byte_count -= len(bytes_read)
            except socket.timeout:
                pass

    def read_float(self):
        """Read a floating point value from the selected device

        :return: The read value as a float.
        """
        sval = self.read()
        try:
            fval = float(GPIB.BAD_CHRS.sub('', sval))
        except ValueError:
            fval = float(sval[:-1])
        return fval

    def read_int(self):
        """Read an integer value from the selected device

        :return: The read value as an integer.
        """
        sval = self.read()
        try:
            ival = int(GPIB.BAD_CHRS.sub('', sval))
        except ValueError:
            ival = int(sval[:-1])
        return ival

    def wait_for_done(self):
        """Wait for 'DONE?' to return non-zero.
        """
        done = 0
        while True:
            try:
                ch = self.read()[0]
                done = int(ch)
                if done:
                    break
            except ValueError:
                pass
            except socket.timeout:
                pass
