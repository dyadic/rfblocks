from rfblocks import hmc833, pe43711
import rpyc


class RFGen(object):

    MIN_FREQUENCY = 25.0
    MAX_FREQUENCY = 6000.0
    MIN_PWR = -22.0
    MAX_PWR = 10.0

    CHANNELS = 2

    def __init__(self, ipaddr, port):
        self._ipaddr = ipaddr
        self._port = port
        self._chan: int = 1

    def initialize(self) -> None:
        self._dev = rpyc.connect(self._ipaddr, self._port,
                                 config={"allow_all_attrs": True})
        self._dev.root.initialize()
        self._chans = self._dev.root.channels

    def reset(self) -> None:
        self._dev.close()

    def local(self) -> None:
        pass

    @property
    def chan(self) -> int:
        """Get or set the output channel to be used.
        """
        return self._chan

    @chan.setter
    def chan(self, ch: int) -> None:
        self._chan = ch + 1

    @property
    def chan_str(self) -> str:
        """Return a string representation of the output channel.
        """
        return 'Chan {}'.format(self.chan)

    @property
    def output(self) -> bool:
        """The output channel state as a :py:`bool`
        """
        return self._chans[self.chan_str].plo_ctl.vco_mute

    @output.setter
    def output(self, state: bool) -> None:
        self._chans[self.chan_str].plo_ctl.vco_mute = not state
        self._dev.root.configure_plo(self.chan_str)

    @property
    def freq(self) -> float:
        """The channel output frequency (in MHz) as a :py:`float`.
        """
        return self._chans[self.chan_str].plo_ctl.freq

    @freq.setter
    def freq(self, f: float) -> None:
        self._chans[self.chan_str].plo_ctl.freq = f
        self._dev.root.configure_plo_freq(self.chan_str)

    @property
    def level(self) -> float:
        """The channel output level as a :py:`float` in dBm.
        """
        return self._chans[self.chan_str].level

    @level.setter
    def level(self, lvl: float) -> None:
        chan = self._chans[self.chan_str]
        chan.level = lvl
        chan.configure()
