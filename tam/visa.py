#

DSA815_ID = 'TCPIP0::192.168.0.70::INSTR'
DSG815_ID = 'TCPIP0::192.168.0.199::INSTR'
DG4162_ID = 'TCPIP0::192.168.0.63::INSTR'


class VisaDevice(object):

    DEFAULT_QUERY_DELAY = 0.5  # sec
    DEFAULT_TIMEOUT = 10000  # millisec

    def __init__(self, visa_rm, visa_id):
        """Create an instance of :py:class:`VisaDevice`.

        :param visa_rm: A reference to a Visa resource manager.
            As returned (for example) by a call to
            :py:`pyvisa.ResourceManager()`.
        :type visa_rm:
        :param visa_id: The ID of the instrument to connect with.
        :type visa_id: str
        """
        self._query_delay = VisaDevice.DEFAULT_QUERY_DELAY
        self._timeout = VisaDevice.DEFAULT_TIMEOUT
        self.visa_rm = visa_rm
        self.visa_id = visa_id
        self.inst = None

    def initialize(self):
        """Open a connection to the selected instrument.

        This will also initialize query delay and timeout values for
        the instrument.  Default values for these are
        :py:`VisaDevice.DEFAULT_QUERY_DELAY` and
        :py:`VisaDevice.DEFAULT_TIMEOUT`.  See also
        the properties :py:`query_delay` and :py:`timeout`.
        """
        try:
            self.inst = self.visa_rm.open_resource(self.visa_id)
        except Exception:
            # self.inst will be None
            pass
        else:
            self.inst.query_delay = self._query_delay
            self.inst.timeout = self._timeout

    def close(self):
        """Close the connection to the selected instrument.
        """
        if self.inst:
            self.inst.close()

    def write(self, cmd):
        """Send a command to the instrument.

        :param cmd: The command to send.
        :type cmd: str
        """
        try:
            self.inst.write(cmd)
        except ConnectionResetError:
            # Retry the write after first re-opening the instrument
            # connection
            self.inst.close()
            self.inst = self.visa_rm.open_resource(self.visa_id)
            self.inst.write(cmd)

    def query(self, cmd):
        """Send a query command to the instrument.

        :param cmd: The command to send.
        :type cmd: str

        :return: The instrument response as a string.
        """
        return self.inst.query(cmd)

    def query_float(self, cmd):
        """Send a query command to the instrument.

        :param cmd: The command to send.
        :type cmd: str

        :return: The instrument response as a float.
        """
        return float(self.inst.query(cmd))

    def query_int(self, cmd):
        """Send a query command to the instrument.

        :param cmd: The command to send.
        :type cmd: str

        :return: The instrument response as an integer.
        """
        return int(self.inst.query(cmd))

    @property
    def query_delay(self):
        """Get or set the query delay.  This is the delay in seconds
        between write and read operations.
        """
        return self._query_delay

    @property
    def timeout(self):
        """Get or set the timeout.  This is the timeout (in millisecs)
        for all instrument I/O operations.
        """
        return self._timeout

    @query_delay.setter
    def query_delay(self, delay):
        self._query_delay = delay
        if self.inst:
            self.inst.query_delay = self._query_delay

    @timeout.setter
    def timeout(self, timeout):
        self._timeout = timeout
        if self.inst:
            self.inst.timeout = self._timeout
