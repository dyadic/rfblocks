#
import socket
import pyvisa

from .gpib import (
    GPIB, PROLOGIX_IPADDR
)
from .dsa815 import (
    DSA815
)
from .dsg815 import (
    DSG815
)
from .dg4162 import (
    DG4162
)
from .hp8560a import (
    HP8560A
)
from .hp11729c import (
    HP11729C
)
from .smhu58 import (
    SMHU58
)
from .ddsgen import (
    DDSGen
)
from .rfgen import (
    RFGen
)
from .pwrmeter import (
    PowerMeter
)


class UnknownInstrumentModelException(Exception):
    def __init__(self, model='Unknown'):
        self.model = model
        self.message = "Unknown instrument model: '{}'".format(model)
        super().__init__(self.message)

class InstrumentInitializeException(Exception):
    def __init__(self, model='Unknown'):
        self.model = model
        self.message = "Can't initialize '{}'".format(model)
        super().__init__(self.message)


SIGGEN_MODELS = {'SMHU58': SMHU58, 'DSG815': DSG815, 'DG4162': DG4162,
                 'DDSGEN': DDSGen, 'RFGen': RFGen}
SA_MODELS = {'HP8560A': HP8560A, 'DSA815': DSA815}
PWRMTR_MODELS = {'HP8560A': HP8560A, 'DSA815': DSA815,
                 'PowerMeter': PowerMeter}

GPIB_MODELS = {'SMHU58': SMHU58, 'HP8560A': HP8560A, 'HP11729C': HP11729C}
VISA_MODELS = {'DSG815': DSG815, 'DSA815': DSA815, 'DG4162': DG4162}
RPYC_MODELS = {'DDSGEN': DDSGen, 'PowerMeter': PowerMeter, 'RFGen': RFGen}

INSTRUMENTS = {**GPIB_MODELS, **VISA_MODELS, **RPYC_MODELS}

""""""
SIGGEN_LIMITS = {'SMHU58': [SMHU58.MIN_FREQUENCY, SMHU58.MAX_FREQUENCY,
                            SMHU58.MIN_PWR, SMHU58.MAX_PWR],
                 'DSG815': [DSG815.MIN_FREQUENCY, DSG815.MAX_FREQUENCY,
                            DSG815.MIN_PWR, DSG815.MAX_PWR],
                 'DDSGEN': [DDSGen.MIN_FREQUENCY, DDSGen.MAX_FREQUENCY,
                            DDSGen.MIN_PWR, DDSGen.MAX_PWR],
                 'DG4162': [DG4162.MIN_FREQUENCY, DG4162.MAX_FREQUENCY,
                            DG4162.MIN_PWR, DG4162.MAX_PWR],
                 'RFGen': [RFGen.MIN_FREQUENCY, RFGen.MAX_FREQUENCY,
                           RFGen.MIN_PWR, RFGen.MAX_PWR]}


class InstrumentMgr(object):

    def __init__(self, gpib_ctl_addr=PROLOGIX_IPADDR):
        self._gpib = None
        self._visa = None

    @property
    def gpib_ctl(self):
        """Returns a reference to the GPIB controller.

        The first access to this property will create and initialize
        an instance of :py:class:`tam.GPIB`.  Subsequent accesses
        return the same instance.
        """
        if self._gpib is None:
            self._gpib = GPIB()
            self._gpib.initialize()
        return self._gpib

    @property
    def visa_rm(self):
        """Returns a reference to the Visa resource manager.

        The first access to this property will create and initialize
        an instance of :py:class:`tam.VisaDevice`.  Subsequent accesses
        return the same instance.
        """
        if self._visa is None:
            self._visa = pyvisa.ResourceManager('@py')
        return self._visa

    def close(self):
        """Close the connection to which ever instrument controller
        is currently in use.
        """
        if self._gpib is not None:
            self._gpib.close()
        if self._visa is not None:
            self._visa.close()

    def open_instrument(self, model, instr_id=None):
        """Open a connection to the specified instrument.

        :param model: The instrument model, for example: 'HP8560A'
        :type model: str
        :param instr_id: The instrument identifier.  The format of this
            identifier will depend on the instrument model.
            For a GPIB instrument the identifier will be an integer,
            for other types of instrument the identifier will be
            a string.
        :type instr_id: str or int

        :return: An instance of the instrument control class.

        :raises: :py:class:`tam.InstrumentInitializeException`,
            :py:class:`tam.UnknownInstrumentModelException`
        """
        if model in GPIB_MODELS:
            instr = GPIB_MODELS[model](self.gpib_ctl, int(instr_id))
            try:
                instr.ident
            except AttributeError:
                pass
            except socket.timeout:
                raise InstrumentInitializeException(model)
            try:
                instr.initialize()
            except AttributeError:
                pass
        elif model in VISA_MODELS:
            instr = VISA_MODELS[model](self.visa_rm, instr_id)
            instr.initialize()
            if instr.inst is None:
                raise InstrumentInitializeException(model)
        elif model in RPYC_MODELS:
            instr = RPYC_MODELS[model](*instr_id.split(':'))
            try:
                instr.initialize()
            except ConnectionError:
                raise InstrumentInitializeException(model)
        else:
            raise UnknownInstrumentModelException(model)
        return instr

    def close_instrument(self, instrument):
        """Close the connection to an instrument.

        :param instrument: An instance of the instrument control class.
        :type instrument: object
        """
        try:
            instrument.reset()
            instrument.local()
        except AttributeError:
            pass
