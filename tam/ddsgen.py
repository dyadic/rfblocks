from rfblocks import ad9913
import rpyc


class DDSGen(object):

    MIN_FREQUENCY = 0.1
    MAX_FREQUENCY = 100.0

    MIN_PWR = -30.0
    MAX_PWR = 6.0

    LEVEL_UNITS = ['DAC', 'MV', 'DBM']

    CHANNELS = 2

    def __init__(self, ipaddr, port):
        self._ipaddr = ipaddr
        self._port = port
        self._units = {'Chan 1': 'DBM',
                       'Chan 2': 'DBM'}
        self._chan: int = 1

    def initialize(self) -> None:
        self._dev = rpyc.connect(self._ipaddr, self._port,
                                 config={"allow_all_attrs": True})
        self._dev.root.initialize()
        self._ctls = self._dev.root.dds_controllers()

    def reset(self) -> None:
        self._dev.close()

    def local(self) -> None:
        pass

    @property
    def chan(self) -> int:
        """Get or set the output channel to be used.
        """
        return self._chan

    @chan.setter
    def chan(self, ch: int) -> None:
        self._chan = ch + 1

    @property
    def chan_str(self) -> str:
        """Return a string representation of the output channel.
        """
        return 'Chan {}'.format(self.chan)

    @property
    def output(self) -> bool:
        """The output channel state as a :py:`bool`
        """
        if self._ctls[self.chan_str].state == ad9913.POWER_UP:
            return True
        else:
            return False

    @output.setter
    def output(self, state: bool) -> None:
        dds = self._ctls[self.chan_str]
        dds.state = ad9913.POWER_UP if state else ad9913.POWER_DOWN
        self._dev.root.configure(self.chan_str)

    @property
    def freq(self) -> float:
        """The channel output frequency (in MHz) as a :py:`float`.
        """
        return self._ctls[self.chan_str].freq

    @freq.setter
    def freq(self, f: float) -> None:
        dds = self._ctls[self.chan_str]
        dds.freq = f
        self._dev.root.configure(self.chan_str)

    @property
    def level(self) -> float:
        """The channel output level as a :py:`float`.
        The units of the output level will depend on which level
        unit has been set for the output channel.
        See :py:`level_units`.
        """
        dds = self._ctls[self.chan_str]
        if self._units[self.chan_str] in ['DBM']:
            return dds.dbm
        elif self._units[self.chan_str] in ['MV']:
            return dds.millivolts
        else:
            return dds.level

    @level.setter
    def level(self, lvl: float) -> None:
        dds = self._ctls[self.chan_str]
        if self._units[self.chan_str] in ['DBM']:
            dds.dbm = lvl
        elif self._units[self.chan_str] in ['MV']:
            dds.millivolts = lvl
        else:
            dds.level = lvl
        self._dev.root.configure(self.chan_str)

    @property
    def level_units(self) -> str:
        """The unit used for the channel output level.
        This is one of: :py:`['DAC', 'MV', 'DBM']`
        """
        return self._units[self.chan_str]

    @level_units.setter
    def level_units(self, unit: str) -> None:
        u = unit.upper()
        if u in DDSGen.LEVEL_UNITS:
            self._units[self.chan_str] = u

    def set_signal_output(self, freq: float, level: float) -> None:
        """Set the channel output frequency and level.

        :param freq: The channel output frequency to set (in MHz)
        :type freq: float
        :param level: The channel output level to set (in the current
             channel level units).
        :type level: float
        """
        dds = self._ctls[self.chan_str]
        dds.freq = freq
        self.level = level
