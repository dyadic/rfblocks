#


class HP11729C(object):

    MAX_FILTER_BAND = 2
    MAX_LOCK_RANGE = 5

    def __init__(self, gpib_controller, gpib_id):
        self.gpib = gpib_controller
        self.gpib_id = gpib_id
        self._band = 1
        self._lock_range = 3

    def cmd(self, cmd):
        self.gpib.gpib_id = self.gpib_id
        self.gpib.write(cmd)

    def local(self):
        self.gpib.gpib_id = self.gpib_id
        self.gpib.local()

    @property
    def ident(self):
        """Returns the instrument ID string.

        Raises
        ------
        socket.timeout   If the instrument is unavailable
        """
        _ident = None
        if self.gpib:
            self.gpib.gpib_id = self.gpib_id
            self.gpib.clear()
            self.gpib.write("?ID")
            _ident = self.gpib.gpib.read()
        return _ident

    @property
    def band(self):
        return self._band

    @property
    def lock_range(self):
        return self._lock_range

    @band.setter
    def band(self, b):
        if (b > 0) and (b < HP11729C.MAX_FILTER_BAND+1):
            self._band = b
            self.cmd("FT{:1d}".format(self._band))

    @lock_range.setter
    def lock_range(self, lvl):
        if (lvl > 0) and (lvl < HP11729C.MAX_LOCK_RANGE+1):
            self._lock_range = lvl
            self.cmd("LK{:1d}".format(self._lock_range))

    def read_after_write(self, b):
        self.gpib.gpib_id = self.gpib_id
        self.gpib.read_after_write(b)

    def set_capture(self, state):
        if state:
            self.cmd("CA1")
        else:
            self.cmd("CA0")

    def reset(self):
        pass

    def initialize(self):
        pass
