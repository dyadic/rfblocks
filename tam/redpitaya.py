from typing import Dict
from dataclasses import dataclass


RP_ADC_MIN_FREQUENCY: float = 0.5
RP_ADC_MAX_FREQUENCY: float = 55.0
RP_ADC_MIN_SPAN: float = 0.0005
RP_ADC_MAX_SPAN: float = RP_ADC_MAX_FREQUENCY-RP_ADC_MIN_FREQUENCY

RP_ADC_MAX_REF_LEVEL: float = 30.0
RP_ADC_MIN_REF_LEVEL: float = -150.0
RP_ADC_REF_LEVEL_STEP: float = 1.0
RP_ADC_MIN_ATT: float = 0.0
RP_ADC_MAX_ATT: float = 31.75
RP_ADC_ATT_STEP: float = 0.25

# Calibration types
BASEBAND_CAL = 1
FREQRESP_CAL = 2
NO_CAL = 3


@dataclass
class Capability():
    """Holds information relating to the capability of a Red Pitaya channel
    parameter.  This information may include one or more of the following
    attributes:

    :param min:  The minimum value for the parameter
    :type min:   float
    :param max:  The maximum value for the parameter
    :type max:   float
    :param step: The amount that a given parameter may be incremented or
                 decremented by.
    :type step:  float

    Examples of parameters would be attenuation, frequency, and reference
    level.
    """
    min: float
    max: float
    step: float


@dataclass
class ChannelCapabilities():
    """Holds information relating to the capabilities of a Red Pitaya ADC channel.
    This information includes the following:

    :param adcdma_channel:   The associated ADC DMA channel in the Red Pitaya
                             PL hardware.
    :type adcdma_channel:    int
    :param adc_attenuation:  Describes the capability of the ADC attenuator in
                             the base band rfblocks Red Pitaya ADC hardware.
    :type adc_attenuation:   Capability
    :param fe_attenuation:   Describes the capability of the ADC attenuator in
                             the RF front end associated with the rfblocks
                             Red Pitaya ADC hardware.  Note that this will set
                             to ``None`` if there is no actual attenuator.
    :type fe_attenuation:    Capability
    :param freq:             Describes the frequency range of the associated
                             channel.
    :type freq:              Capability
    :param cal_freq:         Describes the fequency range to be used when calibrating
                             the associated channel.
    :type cal_freq:          Capability
    :param min_span:         The minimum span for the associated channel.
    :type min_span:          float
    :param reflevel:         Describes the reference level range for the channel
    :type reflevel:          Capability
    :param chan_type:        Specifies the type of the associated channel.
                             This can be one of the following: ``DIRECT``, ``SINGLE``,
                             ``SUPERHET``.  These correspond to base band (i.e. no
                             RF front end), direct down conversion RF front end,
                             super heterodyne down conversion RF front end.
    :type chan_type:         int
    :param bandwidth:        The bandwidth of the channel in MHz.  For example,
                             the bandwidth of a ``DIRECT`` channel will be
                             ``RP_ADC_MAX_FREQUENCY - RP_ADC_MIN_FREQUENCY``
    :type bandwidth:         float
    """
    DIRECT = 0
    SINGLE = 1
    SUPERHET = 2

    adcdma_channel: int
    adc_attenuation: Capability
    fe_attenuation: Capability
    freq: Capability
    cal_freq: Capability
    min_span: float
    reflevel: Capability
    chan_type: int = DIRECT
    bandwidth: float = 0.0
    adc_centre_freq: float = None

    @classmethod
    def from_dict(cls, spec: Dict):

        adc_atten_capability = None
        if spec['adc_attenuation'] is not None:
            adc_atten_capability = Capability(**spec['adc_attenuation'])
        fe_atten_capability = None
        if spec['fe_attenuation'] is not None:
            fe_atten_capability = Capability(**spec['fe_attenuation'])

        return ChannelCapabilities(
            adcdma_channel=spec['adcdma_channel'],
            adc_attenuation=adc_atten_capability,
            fe_attenuation=fe_atten_capability,
            freq=Capability(**spec['freq']),
            cal_freq=Capability(**spec['cal_freq']),
            reflevel=Capability(**spec['reflevel']),
            min_span=spec['min_span'],
            chan_type=(
                spec['chan_type']
                if 'chan_type' in spec else ChannelCapabilities.DIRECT),
            bandwidth=(
                spec['bandwidth']
                if 'bandwidth' in spec else 0.0),
            adc_centre_freq=(
                spec['adc_centre_freq']
                if 'adc_centre_freq' in spec else None)
        )
