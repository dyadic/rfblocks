# Generated from launcher.org
#

from typing import (
    Tuple, List, Dict, Optional
)
from functools import partial
import sys
import os.path
import signal
from pathlib import Path
from argparse import ArgumentParser
import asyncio
from dataclasses import dataclass
import json

from PyQt5.QtWidgets import (
    QApplication, QWidget, QLabel, QGridLayout, QVBoxLayout,
    QHBoxLayout, QPushButton
)

from qasync import (
    QEventLoop, asyncSlot
)

import rpyc
from rpyc.utils.registry import UDPRegistryClient


@dataclass
class SerialDeviceService:
    device: str
    serial_number: str
    port: int
    description: str
    application: str
    app_args: str
    baudrate: int = None
    ip_addr: str = ''
    is_connected: bool = False


class LauncherApp(QWidget):

    def __init__(self, app, devices: Dict, app_path: str) -> None:
        super().__init__()
        self._app = app
        self.devices: Dict = devices
        self.app_path = Path(os.path.expandvars(app_path))
        self._registrar = UDPRegistryClient()
        self._bridge_services = []
        self._app_run_buttons = {}
        self.build_ui()

    @property
    def app(self):
        return self._app

    def close(self):
        for svc in self._bridge_services:
            svc.close()
        self._bridge_services = []

    @property
    def bridge_services(self) -> Tuple:
        self.close()
        for svc_addr in self._registrar.discover("bridge"):
            self._bridge_services.append(
                rpyc.connect(*svc_addr))
        return self._bridge_services

    @property
    def devices(self) -> Dict:
        return self._devices

    @devices.setter
    def devices(self, d: Dict) -> None:
        self._devices = {ser: SerialDeviceService(
            '', ser, dev['port'], dev['description'], dev['application'],
            dev['app_args']) for ser, dev in d.items()}

    def query_bridges(self):
        """Update serial device dispositions from bridge service(s)"""
        for dev in self.devices.values():
            dev.device = ''
            dev.ip_addr = ''
        for bridge in self.bridge_services:
            ip_addr, _ = bridge._channel.stream.sock.getpeername()
            for sernum, devinfo in bridge.root.connected_devices.items():
                self.devices[sernum].device = devinfo[0]
                self.devices[sernum].baudrate = devinfo[1]
                self.devices[sernum].port = devinfo[2]
                self.devices[sernum].ip_addr = ip_addr

    def update_devices(self):
        self.query_bridges()
        for sernum, dev in self.devices.items():
            run_btn = self._app_run_buttons[sernum]
            if dev.is_connected:
                run_btn.setEnabled(False)
            elif dev.device and dev.application:
                run_btn.setEnabled(True)
            else:
                run_btn.setEnabled(False)

    @asyncSlot()
    async def run_app(self, serial_num, checked):
        dev = self.devices[serial_num]
        run_btn = self._app_run_buttons[serial_num]
        app_path = self.app_path / dev.application
        app_args = [app_path, '-d', f'socket://{dev.ip_addr}:{dev.port}']
        if dev.app_args is not None and len(dev.app_args):
            app_args += dev.app_args.split()
        proc = await asyncio.create_subprocess_exec(
            'python', *app_args, cwd=self.app_path)
        run_btn.setEnabled(False)
        dev.is_connected = True
        await proc.wait()
        dev.is_connected = False
        run_btn.setEnabled(True)

    def show_info(self, serial_num, checked):
        print('show_info:')
        dev = self.devices[serial_num]
        print(f'{dev=}')

    def build_ui(self):
        vbox = QVBoxLayout()
    
        vbox.addLayout(self.create_device_layout())
        hbox = QHBoxLayout()
        hbox.addStretch()
        btn = QPushButton('Refresh')
        btn.clicked.connect(self.update_devices)
        hbox.addWidget(btn)
        btn = QPushButton('Close')
        btn.clicked.connect(self.app.quit)
        hbox.addWidget(btn)
        vbox.addLayout(hbox)
    
        self.app.aboutToQuit.connect(self.close)
        self.setLayout(vbox)
        self.setWindowTitle('rfblocks App Launcher')
    
    def create_device_buttons(self, serial_number):
        dev_service = self.devices[serial_number]
        btn_layout = QGridLayout()
        info_btn = QPushButton('Info')
        info_btn.clicked.connect(partial(self.show_info, serial_number))
        run_btn = QPushButton('Run App.')
        if (not dev_service.device) or (not dev_service.application):
            run_btn.setEnabled(False)
        run_btn.clicked.connect(partial(self.run_app, serial_number))
        btn_layout.addWidget(info_btn, 0, 0)
        btn_layout.addWidget(run_btn, 0, 1)
        btn_layout.setHorizontalSpacing(2)
        btn_layout.setVerticalSpacing(0)
        self._app_run_buttons[serial_number] = run_btn
        return btn_layout
    
    def create_device_layout(self):
        self.query_bridges()
        self._dev_grid_layout = QGridLayout()
        # Display details for all known devices
        for i, (serial_num, serial_srv_info) in enumerate(self.devices.items()):
            hbox = QHBoxLayout()
            hbox.addStretch()
            hbox.addWidget(QLabel(f'{serial_srv_info.description}:'))
            self._dev_grid_layout.addLayout(hbox, i, 0)
            self._dev_grid_layout.addLayout(
                self.create_device_buttons(serial_num), i, 1)
        self._dev_grid_layout.setVerticalSpacing(0)
        return self._dev_grid_layout


def main():

    default_known_devices = '../rfblocks/bridge.json'
    default_app_path = '${RFBLOCKS_REFDESIGNS}'
    
    parser = ArgumentParser(description=
                            'rfblocks Application Launcher.')
    
    parser.add_argument("-C", "--config", default=default_known_devices,
                        help="File containing known devices configuration")
    parser.add_argument("-P", "--app_path", default=default_app_path,
                        help="Dir containing device applications")
    args = parser.parse_args()

    # This ensures that Cntl-C will work as expected:
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    app = QApplication(sys.argv)
    loop = QEventLoop(app)
    asyncio.set_event_loop(loop)
    known_devices = None
    with open(args.config) as fd:
        known_devices = json.load(fd)
    launcher_app = LauncherApp(app, known_devices,
                               args.app_path)
    launcher_app.show()

    with loop:
        loop.set_debug(True)
        sys.exit(app.exec_())


if __name__ == '__main__':
    main()
