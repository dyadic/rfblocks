#!/usr/bin/env python
#
#  virt_ecapp.py
#
#  A barebones simulation of the ECApp firmware.
#  Intended for testing.
#
#  This script should be used with socat as follows:
#
#    socat PTY,link=./virtual-tty,raw,echo=0 EXEC:virt_ecapp.py
#
#  This then allows ./virtual-tty to be used as a serial device
#  (with default baudrate 38400).  Commands can be sent via this
#  serial device and virt-ecapp will return the appropriate
#  responses.

import sys
from enum import Enum
from argparse import ArgumentParser

class CmdStates(Enum):
    AWAITING = 1
    AVAILABLE = 2
    PROCESSED = 3

running = True

def stop(signum, frame):
    global running
    running = False

    
def main(stderr_fd):
    cmd_buffer = ''
    cmd_state = CmdStates.AWAITING

    while running:
        if cmd_state in [CmdStates.AWAITING]:
            ch = ''
            while ch not in ['\r']:
                ch = sys.stdin.read(1)
                cmd_buffer += ch
            cmd_state = CmdStates.AVAILABLE

        elif cmd_state in [CmdStates.AVAILABLE]:
            stderr_fd.write("Cmd: {}\n".format(cmd_buffer))
            stderr_fd.flush()
            resp_msg = ''
            if cmd_buffer[0] in ['p', 'P']:
                resp_msg += '1'
            resp_msg += '.'
            cmd_state = CmdStates.PROCESSED

        elif cmd_state in [CmdStates.PROCESSED]:
            stderr_fd.write("Resp: {}\n".format(resp_msg))
            stderr_fd.flush()
            sys.stdout.write(resp_msg)
            sys.stdout.flush()
            cmd_buffer = ''
            cmd_state = CmdStates.AWAITING

    stderr_fd.write("Shutting down.\n")
    stderr_fd.flush()
    sys.exit(0)

if __name__ == '__main__':
    parser = ArgumentParser(description=
                            'Virtual ECApp instance for testing purposes')
    parser.add_argument("-O", "--serial_log", default=None,
                        help='Log serial cmds to this file')
    args = parser.parse_args()

    if args.serial_log:
        with open(args.serial_log, 'w') as cmd_fd:
            main(cmd_fd)
    else:
        main(sys.stderr)
