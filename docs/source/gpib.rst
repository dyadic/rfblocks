.. role:: py(code)
   :language: py
   :class: highlight

.. _gpib:

GPIB
====

.. autoclass:: tam.GPIB
    :members:
