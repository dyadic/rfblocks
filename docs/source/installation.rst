.. _installation:

Installing ``rfblocks``
=======================

It's recommended (although not mandatory) that the ``rfblocks`` module
be installed in a Python virtual environment.  For more detail about
creating virtual environments please consult :ref:`pyenv-virtualenvs`.

The package can be installed "manually" as follows:

.. code:: sh

    git clone git@gitlab.com:dyadic/rfblocks.git
    cd rfblocks
    pip install .

Test the install:

.. code:: sh

    python
    >>> import rfblocks
    >>> rfblocks.__version__
    '0.1.0'
    >>>

Now have a look at :ref:`quickstart`.

.. _pyenv-virtualenvs:

Using ``pyenv`` to Manage Virtual Environments
----------------------------------------------

`Pyenv <https://github.com/pyenv/pyenv>`_ enables easy switching between
multiple versions of Python and together with the `pyenv-virtualenv
<https://github.com/pyenv/pyenv-virtualenv>`_ plugin allows Python
virtual environments to run arbitrary Python versions.

The easiest way to get ``pyenv`` and the associated plugins installed is
to use `pyenv-installer <https://github.com/pyenv/pyenv-installer>`_:

.. code:: sh

    curl https://pyenv.run | bash

This will install ``pyenv`` along with some useful plugins including
``pyenv-virtualenv``.  At the end of the run something like the
following should be shown:

.. code:: sh

    WARNING: seems you still have not added 'pyenv' to the load path.

    # Load pyenv automatically by adding
    # the following to ~/.bashrc:

    export PATH="$HOME/.pyenv/bin:$PATH"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"

The exact output will depend on the shell in use.  The instructions
should be followed and when done the shell needs to be reloaded:

.. code:: sh

    exec "$SHELL" # Or just restart your terminal

Install a Python version
^^^^^^^^^^^^^^^^^^^^^^^^

.. code:: sh

    pyenv install -v 3.8.8

Create a new virtual environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code:: sh

    pyenv virtualenv 3.8.8 rfblocks

Install ``rfblocks`` into the virtual environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Ensure that ``pip`` is up to date:

.. code:: sh

    pyenv activate rfblocks
    python -m pip install --upgrade pip

Now clone and install rfblocks and dependencies:

.. code:: sh

    git clone git@gitlab.com:dyadic/rfblocks.git
    cd rfblocks
    pip install .
