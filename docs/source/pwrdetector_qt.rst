.. _pwrdetector_qt:

.. role:: py(code)
   :language: py
   :class: highlight

pwrdetector_qt
==============

PowerDetector
-------------

.. figure:: PowerDetector.png
    :align: center
    :figwidth: 50%

.. autoclass:: qtrfblocks.PowerDetector
    :members:

PwrCalibrationDialog
--------------------

.. figure:: PwrCalibrationDialog.png
    :align: center
    :figwidth: 50%

.. autoclass:: qtrfblocks.PwrCalibrationDialog
    :members:


