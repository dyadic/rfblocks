.. _pe43711_qt:

.. role:: py(code)
   :language: py
   :class: highlight

pe43711_qt
==========

StepAttenuator
--------------

.. figure:: StepAttenuator.png
    :align: center
    :figwidth: 50%

.. autoclass:: qtrfblocks.StepAttenuator
    :members:
