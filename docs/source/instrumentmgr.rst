.. role:: py(code)
   :language: py
   :class: highlight

.. _instrumentmgr:

InstrumentMgr
=============

.. autoclass:: tam.InstrumentMgr
    :members:
