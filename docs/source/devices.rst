.. _devices:

Device API
==========

.. toctree::
    :maxdepth: 1

    ad9552
    ad9913
    hmc833
    log_detector
    pe43711
    pe42420
    redpitaya

