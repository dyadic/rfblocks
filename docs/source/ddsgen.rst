.. role:: py(code)
   :language: py
   :class: highlight

.. _DDSGen:

DDSGen
======

.. autoclass:: tam.DDSGen
    :members:
