.. role:: py(code)
   :language: py
   :class: highlight

.. _PwrDetectorController:

PwrDetectorController
=====================

.. autoclass:: rfblocks.PwrDetectorController
    :members:

