.. role:: py(code)
   :language: py
   :class: highlight

.. _AD9913Controller:

AD9913Controller
================

.. automodule:: rfblocks.ad9913_controller
    :members:
