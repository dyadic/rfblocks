.. role:: py(code)
   :language: py
   :class: highlight

.. _visa:

VisaDevice
==========

.. autoclass:: tam.VisaDevice
    :members:
