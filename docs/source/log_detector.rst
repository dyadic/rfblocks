.. role:: py(code)
   :language: py
   :class: highlight

.. _log_detector:

LogDetector
===========

.. autoclass:: rfblocks.LogDetector
    :members:

