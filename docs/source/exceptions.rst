.. role:: py(code)
   :language: py
   :class: highlight

.. _exceptions:

Exceptions
==========

FrequencyRangeException
-----------------------

.. autoclass:: rfblocks.FrequencyRangeException
    :members:

ModulusConstraintException
--------------------------

.. autoclass:: rfblocks.ModulusConstraintException
    :members:

AttenuationRangeException
-------------------------

.. autoclass:: rfblocks.AttenuationRangeException
    :members:

CalibrationDataError
--------------------

.. autoclass:: rfblocks.CalibrationDataError
    :members:

CalibrationRangeError
---------------------

.. autoclass:: rfblocks.CalibrationRangeError
    :members:

InstrumentInitializeException
-----------------------------

.. autoclass:: tam.InstrumentInitializeException
    :members:

UnknownInstrumentModelException
-------------------------------

.. autoclass:: tam.UnknownInstrumentModelException
    :members:
