
A Python Package for ``rfblocks`` Hardware Control
==================================================

The ``rfblocks`` package provides a convenient way to control RF Blocks
hardware modules.  Although ``rfblocks`` is intended to be used with the
ECApp embedded firmware it is also possible to use it with other SPI
control hardware.  See :ref:`quickstart` for further details.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   installation
   quickstart
   devices
   devicecontrollers
   devicegui
   serialbridge
   instrumentcontrol
   exceptions
   developers

   license

