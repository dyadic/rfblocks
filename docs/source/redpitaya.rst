.. role:: py(code)
   :language: py
   :class: highlight

.. _redpitaya:

redpitaya
=========

.. autoclass:: tam.ChannelCapabilities

.. autoclass:: tam.Capability

