.. _ad9913_qt:

.. role:: py(code)
   :language: py
   :class: highlight

ad9913_qt
=========

DDSChan
-------

.. figure:: DDSChan.png
    :align: center
    :figwidth: 50%

.. autoclass:: qtrfblocks.DDSChan
    :members:

SweepDialog
-----------

.. figure:: SweepDialog.png
    :align: center
    :figwidth: 50%

.. autoclass:: qtrfblocks.SweepDialog
    :members:

ProfilesDialog
--------------

.. figure:: ProfilesDialog.png
    :align: center
    :figwidth: 80%

.. autoclass:: qtrfblocks.ProfilesDialog
    :members:
