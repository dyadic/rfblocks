.. role:: py(code)
   :language: py
   :class: highlight

.. _instrumentcontrol:

Instrument Control
==================

Instrument control classes are contained in the ``tam`` package.
Here is a brief example illustrating the usage:

.. code:: python

    import sys
    from tam import (
        InstrumentMgr, InstrumentInitializeException,
        UnknownInstrumentModelException
    )

    mgr = InstrumentMgr()
    try:
        siggen = mgr.open_instrument('DSG815', 'TCPIP0::192.168.0.199::INSTR')
        sa = mgr.open_instrument('HP8560A', 18)
    except InstrumentInitializeException as iie:
        print(iie.message)
        sys.exit(-1)
    except UnknownInstrumentModelException as ume:
        print(ume.message)
        sys.exit(-1)

    siggen.set_signal_output(1000, 0.0)
    sa.measure_pwr(1000)

Available device models can be listed as follows:

.. code:: python

    from tam import manager
    list(manager.INSTRUMENTS)
    >>> ['SMHU58', 'HP8560A', 'HP11729C', 'DSG815', 'DSA815', 'DG4162',
    'DDSGEN', 'PowerMeter']


``rfblocks`` Based Instruments
------------------------------

.. toctree::
    :maxdepth: 1

    ddsgen
    pwrmeter
    sadisplay

Third Party Instruments
-----------------------

.. toctree::
    :maxdepth: 1

    dg4162
    dsg815
    smhu58
    dsa815
    hp8560a

Middleware Classes
------------------

.. toctree::
    :maxdepth: 1

    instrumentmgr
    gpib
    visa
    

Adding New Instrument Models
----------------------------

Device models will fall into one of three categories depending on which type
of instrument control they support:

.. code:: python

    list(manager.GPIB_MODELS)
    >>> ['SMHU58', 'HP8560A', 'HP11729C']
    list(manager.VISA_MODELS)
    >>> ['DSG815', 'DSA815', 'DG4162']
    list(manager.RPYC_MODELS)
    >>> ['DDSGEN', 'PowerMeter']

