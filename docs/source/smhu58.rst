.. role:: py(code)
   :language: py
   :class: highlight

.. _SMHU58:

SMHU58
======

.. autoclass:: tam.SMHU58
    :members:
