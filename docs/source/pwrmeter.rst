.. role:: py(code)
   :language: py
   :class: highlight

.. _PowerMeter:

PowerMeter
==========

.. autoclass:: tam.PowerMeter
    :members:
