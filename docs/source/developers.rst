.. role:: py(code)
   :language: py
   :class: highlight

.. _developers:

Developers
==========

Testing Changes
---------------

Unit testing of the ``rfblocks`` module is done using `doctest
<https://docs.python.org/3/library/doctest.html>`_ and `pytest
<https://docs.pytest.org/en/7.0.x/>`_.

Using doctest
^^^^^^^^^^^^^

In general, doctest is used to test the low level device modules such as
:py:class:`rfblocks.ad9552` or :py:class:`rfblocks.hmc833`.  Tests are executed
by running the module as a script.  For example:

.. code:: sh

    pyenv activate rfblocks
    cd software/rfblocks/rfblocks
    python ad9552.py
    python ad9913.py
    ...

Nothing will be displayed unless a test example fails in which case the
failing examples and the causes of the failures are printed to stdout.
Running the modules with the ``-v`` switch will generate detailed reports for
each of the test examples.

Using pytest
^^^^^^^^^^^^

Unit testing of device controller modules is done using pytest.  This allows
the use of a test fixture which simulates the serial connection to actual
``rfblocks`` hardware.  Tests are executed by running ``pytest``:

.. code:: sh

    pyenv activate rfblocks
    cd software/rfblocks/rfblocks/tests
    pytest test_ad9552_controller.py
    ...

Output will look similar to the following:

.. code:: sh

    ==================================== test session starts ====================================
    platform darwin -- Python 3.8.5, pytest-6.2.4, py-1.10.0, pluggy-0.13.1
    PyQt5 5.15.0 -- Qt runtime 5.15.0 -- Qt compiled 5.15.0
    rootdir: /Users/timm/Documents/Electronics/Designs/ProdGitRepos/software/rfblocks
    plugins: qt-4.0.2
    collected 11 items
    
    test_ad9552_controller.py ...........                                        [100%]

    ========================================= 11 passed in 21.03s ===================================
  
