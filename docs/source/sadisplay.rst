.. role:: py(code)
   :language: py
   :class: highlight

.. _SADisplay:

SADisplay
=========

.. autoclass:: tam.SADisplay
    :members:
