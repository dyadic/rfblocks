.. role:: py(code)
   :language: py
   :class: highlight

.. _AD9552Controller:

AD9552Controller
================

.. autoclass:: rfblocks.AD9552Controller
    :members:
       
.. _AD9552Channel:

AD9552Channel
=============

.. autoclass:: rfblocks.AD9552Channel
    :members:
